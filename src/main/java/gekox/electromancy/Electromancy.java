package gekox.electromancy;

import gekox.electromancy.proxy.IProxy;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MODID, name = Reference.NAME, version = Reference.VERSION, dependencies = Reference.DEPENDENTS)
public class Electromancy {

	@Mod.Instance
	public static Electromancy instance = new Electromancy();

	@SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
	public static IProxy proxy;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {

		LogHelper.info("Bzzt bzzt let's go!");

		proxy.preInit(event);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

		LogHelper.info("Bzzt bzzt going!");

		proxy.init(event);
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {

		LogHelper.info("Bzzt bzzt done!");

		proxy.postInit(event);
	}


}
