package gekox.electromancy.api.crafting;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.*;
import gekox.electromancy.reference.Reference;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.IRecipeFactory;
import net.minecraftforge.common.crafting.JsonContext;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Geko_X on 30/06/2017.
 *
 */
public class LightningCraftingFactory implements IRecipeFactory {

	@Override
	public IRecipe parse(JsonContext context, JsonObject json) {
		LightningRecipe recipe = LightningRecipe.factory(context, json);

		LightningPrimer primer = new LightningPrimer();
		primer.mirrored = JsonUtils.getBoolean(json, "mirrored", true);
		primer.input = recipe.getIngredients();

		return new LightningRecipe(new ResourceLocation(Reference.MODID, "lightning_crafting"), recipe.getRecipeOutput(), primer);
	}

	public static class LightningPrimer extends CraftingHelper.ShapedPrimer {

		public double energyRequired = 0;

		public LightningPrimer() {
			super();
		}
	}

	public static class LightningRecipe implements IRecipe {

		private ItemStack output;
		private List<ItemStack> inputs;
		private NonNullList<Ingredient> ingredientList = NonNullList.create();

		private double energyRequired;
		private ResourceLocation group;

		public LightningRecipe(ResourceLocation group, ItemStack result, LightningPrimer primer) {

			this.group = group;
			this.output = result;
			this.energyRequired = primer.energyRequired;

			List<ItemStack> finalInputs = new ArrayList<>();

			for (Object obj : primer.input) {
				if(obj instanceof ItemStack) {
					finalInputs.add((ItemStack)obj);
				}

				if(obj instanceof Item) {
					finalInputs.add(new ItemStack((Item)obj));
				}

				if(obj instanceof Block) {
					finalInputs.add(new ItemStack((Block)obj));
				}

				// Todo: Oredict strings
			}

			this.inputs = new ArrayList<>(finalInputs);

		}

		@Override
		public boolean matches(InventoryCrafting inv, World worldIn) {
			return false;
		}

		// From ShapedOreRecipe, somewhere in the forge code
		public static LightningRecipe factory(JsonContext context, JsonObject json) {

			String group = JsonUtils.getString(json, "group", "");

			NonNullList<Ingredient> ings = NonNullList.create();
			for (JsonElement ele : JsonUtils.getJsonArray(json, "ingredients"))
				ings.add(CraftingHelper.getIngredient(ele, context));

			if (ings.isEmpty())
				throw new JsonParseException("No ingredients for lightning recipe");

			LightningPrimer primer = new LightningPrimer();
			primer.width = 0;
			primer.height = 0;
			primer.mirrored = false;
			primer.input = ings;
			primer.energyRequired = JsonUtils.getFloat(json, "energy", 0f);


			ItemStack result = CraftingHelper.getItemStack(JsonUtils.getJsonObject(json, "result"), context);
			return new LightningRecipe(group.isEmpty() ? null : new ResourceLocation(group), result, primer);
		}

		@Override
		public ItemStack getCraftingResult(InventoryCrafting inv) {
			return output.copy();
		}

		@Override
		public boolean canFit(int width, int height) {
			return true;
		}

		@Override
		public ItemStack getRecipeOutput() {
			return this.output;
		}

		@Override
		public NonNullList<Ingredient> getIngredients() {
			return new NonNullList<Ingredient>(this.inputs);
		}

		@Override
		public String toString() {
			return String.format("Lightning Recipe: result=%s p=%.2f input=[%s]", this.output, this.energyRequired, this.inputs.toString());
		}

		@Override
		public IRecipe setRegistryName(ResourceLocation name) {
			return null;
		}

		@Nullable
		@Override
		public ResourceLocation getRegistryName() {
			return null;
		}

		@Override
		public Class<IRecipe> getRegistryType() {
			return null;
		}
	}
}
