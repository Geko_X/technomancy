package gekox.electromancy.api.crafting;

import gekox.electromancy.util.LogHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Geko_X on 23/10/2016.
 *
 */
public class LightningCraftingRegistry {

	private static List<LightningRecipe> recipies = new ArrayList<>();

	public static void registerRecipe(LightningRecipe recipe) {
		LogHelper.info("Registering lightning recipe: " + recipe.toString());
		recipies.add(recipe);
	}

	public static List<LightningRecipe> getRecipies() {
		return new ArrayList<>(recipies);
	}

}
