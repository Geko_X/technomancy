package gekox.electromancy.api.crafting;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Geko_X on 23/10/2016.
 *
 */
public class LightningRecipe {

	private ItemStack output;
	private List<ItemStack> inputs;
	private double potentialRequired;

	public LightningRecipe(ItemStack output, double potentialRequired, Object... inputs) {

		this.output = output;
		this.potentialRequired = potentialRequired;

		List<ItemStack> finalInputs = new ArrayList<>();

		for (Object obj : inputs) {
			if(obj instanceof ItemStack) {
				finalInputs.add((ItemStack)obj);
			}

			if(obj instanceof Item) {
				finalInputs.add(new ItemStack((Item)obj));
			}

			if(obj instanceof Block) {
				finalInputs.add(new ItemStack((Block)obj));
			}

			// Todo: Oredict strings
		}

		this.inputs = new ArrayList<>(finalInputs);

	}

	public boolean matches(List<ItemStack> items, double potential) {

		if(this.potentialRequired >= potential) {
			return false;
		}

		List<ItemStack> recipe = this.getRecipeInput();

		for(ItemStack stack : items) {
			for(ItemStack stack2 : recipe) {
				if(ItemStack.areItemStacksEqual(stack, stack2)) {
					recipe.remove(stack2);
					break;
				}
			}
		}

		return recipe.isEmpty();

	}

	public ItemStack getRecipeOutput() {
		return this.output.copy();
	}

	public List<ItemStack> getRecipeInput() {
		return new ArrayList<>(this.inputs);
	}

	public double getPotential() {
		return this.potentialRequired;
	}

	@Override
	public String toString() {
		return String.format("Lightning Recipe: result=%s p=%.2f input=[%s]", this.output, this.potentialRequired, this.inputs.toString());
	}

}
