package gekox.electromancy.api.power;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * ElectromancyPowerStorage implementation
 *
 */
public class ElectromancyPowerStorage implements IElectromancyPowerStorage {

	private double currentPower;
	private double maxPower;

	public ElectromancyPowerStorage() {
		this(10000);
	}

	public ElectromancyPowerStorage(double maxPower) {
		this.currentPower = 0;
		this.maxPower = maxPower;

	}

	/* TechnomancyPower */

	@Override
	public double getCurrentPower() {
		return this.currentPower;
	}

	@Override
	public void setCurrentPower(double power) {
		this.currentPower = power;
	}

	@Override
	public double getMaxPower() {
		return this.maxPower;
	}

	@Override
	public void setMaxPower(double power) {
		this.maxPower = power;
	}

	@Override
	public double receivePower(double energy) {

		energy = energy > 0 ? energy : 0;
		
		double received = Math.min(this.maxPower - this.currentPower, energy);
		this.currentPower += received;

		return received;

	}

	@Override
	public double extractPower(double energy) {

		energy = energy > 0 ? energy : 0;
		
		double extracted = Math.min(this.currentPower, energy);
		this.currentPower -= extracted;

		return extracted;

	}
	
	public double getEnergyPercent() {
		return this.currentPower / this.maxPower;
	}
}
