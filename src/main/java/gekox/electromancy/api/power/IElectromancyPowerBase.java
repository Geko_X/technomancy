package gekox.electromancy.api.power;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * Interface for all things that common to both TechnomanyPower producers and comsumers
 *
 */
public interface IElectromancyPowerBase {

	/* Getters, setters */

	/**
	 * Gets the current amount of power this thing has
	 */
	double getCurrentPower();

	/**
	 * Sets the current amount of power
	 * @param power: The amount to set to
	 */
	void setCurrentPower(double power);

	/**
	 * Gets the maximum amount of power this thing can store
	 */
	double getMaxPower();

	/**
	 * Sets the maximum amount of power that this thing can store
	 * @param power: The maximum amount of power to store
	 */
	void setMaxPower(double power);

}
