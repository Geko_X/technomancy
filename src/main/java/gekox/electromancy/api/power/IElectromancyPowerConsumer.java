package gekox.electromancy.api.power;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * Interface for all things that use TechnomanyPower
 *
 */
public interface IElectromancyPowerConsumer extends IElectromancyPowerBase {

	/**
	 * Send power to this thing
	 *
	 * @param power: The amount of power to receive. If there is not enough storage space for this amount of power,
	 *                the amount actually stored is stored
	 * @return: The amount of power actually stored
	 */
	double receivePower(double power);

}
