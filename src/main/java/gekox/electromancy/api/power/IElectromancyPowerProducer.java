package gekox.electromancy.api.power;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * Interface for all things that use TechnomanyPower
 *
 */
public interface IElectromancyPowerProducer extends IElectromancyPowerBase {

	/**
	 * Extracts power from this thing
	 *
	 * @param power: The amount of power to extract. If there is less than this much in storage, the storage amount is returned
	 * @return: The amount of power actually extracted
	 */
	double extractPower(double power);

}
