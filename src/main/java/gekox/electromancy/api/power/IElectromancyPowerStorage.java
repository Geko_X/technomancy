package gekox.electromancy.api.power;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * Storage of TechnomancyPower
 *
 */
public interface IElectromancyPowerStorage extends IElectromancyPowerConsumer, IElectromancyPowerProducer {



}
