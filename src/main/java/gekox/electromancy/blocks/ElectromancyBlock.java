package gekox.electromancy.blocks;

import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * Base block class
 *
 */
public abstract class ElectromancyBlock extends Block {

	public ElectromancyBlock(Material materialIn) {
		super(materialIn);
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);
	}

	public ElectromancyBlock() {
		this(Material.ROCK);
	}

	// Overrides for the name
	@Override
	public String getUnlocalizedName() {
		return String.format("tile.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

	@SideOnly(Side.CLIENT)
	public void initModel() {
		LogHelper.info("Registering model for " + this.toString());
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}

}
