package gekox.electromancy.blocks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public abstract class ElectromancyMetaBlock extends ElectromancyBlock {
	
	private int numOfItems;
	public String[] names = {
			"0",
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"10",
			"11",
			"12",
			"13",
			"14",
			"15"
	};
	
	public ElectromancyMetaBlock(int items) {
		this.numOfItems = items;
	}
	
	public ElectromancyMetaBlock() {
		this(16);
	}
	
	public String getUnlocalizedName(ItemStack stack) {
		return String.format("item.%s_%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[stack.getItemDamage()]);
	}
	
	public String getUnlocalizedName(int meta) {
		return String.format("item.%s_%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[meta]);
	}
	
	@Override
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
	public int getNumOfItems() {
		return numOfItems;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items) {
		for (int i = 0; i < numOfItems; i ++) {
			items.add(new ItemStack(this, 1, i));
		}
	}
	
}
