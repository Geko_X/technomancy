package gekox.electromancy.blocks;

import gekox.electromancy.api.power.IElectromancyPowerStorage;
import gekox.electromancy.api.power.ElectromancyPowerStorage;
import gekox.electromancy.reference.Reference;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;

/**
 * Created by Geko_X on 22/10/2016.
 *
 */
public abstract class ElectromancyPowerTile extends ElectromancyTile implements IElectromancyPowerStorage, ITickable {

	protected ElectromancyPowerStorage storage;
	protected int currentTick = 0;

	public ElectromancyPowerTile() {
		this(10000);
	}

	public ElectromancyPowerTile(double amount) {
		storage = new ElectromancyPowerStorage(amount);
	}

	//===========================================
	//                   POWER
	//===========================================

	@Override
	public double getCurrentPower() {
		return storage.getCurrentPower();
	}

	@Override
	public void setCurrentPower(double power) {
		storage.setCurrentPower(power);
	}

	@Override
	public double getMaxPower() {
		return storage.getMaxPower();
	}

	@Override
	public void setMaxPower(double power) {
		storage.setMaxPower(power);
	}

	@Override
	public double extractPower(double power) {
		return storage.extractPower(power);
	}

	@Override
	public double receivePower(double power) {
		return storage.receivePower(power);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);

		if(nbt.hasKey(Reference.NBT_POWER_MAX)) {
			this.setCurrentPower(nbt.getDouble(Reference.NBT_POWER_MAX));
		}

		if(nbt.hasKey(Reference.NBT_POWER)) {
			this.setCurrentPower(nbt.getDouble(Reference.NBT_POWER));
		}

	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);

		nbt.setDouble(Reference.NBT_POWER_MAX, this.getMaxPower());
		nbt.setDouble(Reference.NBT_POWER, this.getCurrentPower());

		return nbt;

	}

}
