package gekox.electromancy.blocks;

import gekox.electromancy.blocks.material.BlockVoltaicIron;
import gekox.electromancy.blocks.relay.BlockRelay;
import gekox.electromancy.blocks.relay.TileRelay;
import gekox.electromancy.blocks.simpleGenerator.BlockSimpleGenerator;
import gekox.electromancy.blocks.simpleGenerator.TileSimpleGenerator;
import gekox.electromancy.blocks.storage.BlockStorage;
import gekox.electromancy.blocks.storage.TileStorage;
import gekox.electromancy.reference.Names;
import gekox.electromancy.util.LogHelper;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Geko_X on 10/10/2016.
 *
 */
public class ModBlocks {

	// Block
	@GameRegistry.ObjectHolder("electromancy:voltaic_iron_block")
	public static BlockVoltaicIron blockVoltaicIron;
	
	// Tile
	@GameRegistry.ObjectHolder("electromancy:relay")
	public static BlockRelay blockRelay;
	@GameRegistry.ObjectHolder("electromancy:storage")
	public static BlockStorage blockStorage;
	@GameRegistry.ObjectHolder("electromancy:simple_generator")
	public static BlockSimpleGenerator blockSimpleGenerator;
	
	public static void init() {

	}

	public static void registerBlock(RegistryEvent.Register<Block> event) {
		event.getRegistry().register(new BlockVoltaicIron());
		event.getRegistry().register(new BlockRelay());
		event.getRegistry().register(new BlockStorage());
		event.getRegistry().register(new BlockSimpleGenerator());

		GameRegistry.registerTileEntity(TileStorage.class, Names.Blocks.STORAGE + "_tile");
		GameRegistry.registerTileEntity(TileRelay.class, Names.Blocks.RELAY + "_tile");
		GameRegistry.registerTileEntity(TileSimpleGenerator.class, Names.Blocks.SIMPLE_GENERATOR + "_tile");

	}

	public static void registerItemBlock(RegistryEvent.Register<Item> event) {
		event.getRegistry().register(new ItemBlock(blockVoltaicIron).setRegistryName(blockVoltaicIron.getRegistryName()));
		event.getRegistry().register(new ItemBlock(blockRelay).setRegistryName(blockRelay.getRegistryName()));
		event.getRegistry().register(new ItemBlock(blockStorage).setRegistryName(blockStorage.getRegistryName()));
		event.getRegistry().register(new ItemBlock(blockSimpleGenerator).setRegistryName(blockSimpleGenerator.getRegistryName()));

	}

	@SideOnly(Side.CLIENT)
	public static void initRenders() {

		// Block
		blockVoltaicIron.initModel();
		
		// Tile
		blockRelay.initModel();
		blockStorage.initModel();
		blockSimpleGenerator.initModel();
	}

}
