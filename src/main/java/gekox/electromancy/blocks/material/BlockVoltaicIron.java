package gekox.electromancy.blocks.material;

import gekox.electromancy.blocks.ElectromancyBlock;
import gekox.electromancy.reference.Names;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public class BlockVoltaicIron extends ElectromancyBlock {
	
	public BlockVoltaicIron() {
		super(Material.IRON);
		
		this.setRegistryName(Names.Blocks.VOLTAIC_IRON_BLOCK);
		this.setUnlocalizedName(Names.Blocks.VOLTAIC_IRON_BLOCK);
		this.setHardness(1.5f);
		this.setHarvestLevel("pickaxe", 2);
		
	}
}
