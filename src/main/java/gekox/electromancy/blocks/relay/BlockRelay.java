package gekox.electromancy.blocks.relay;

import gekox.electromancy.blocks.ElectromancyBlock;
import gekox.electromancy.blocks.ElectromancyContainer;
import gekox.electromancy.reference.Names;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Geko_X on 18/10/2016.
 *
 * These send fulmen around to other nearby fulmen blocks or things
 */
public class BlockRelay extends ElectromancyContainer {
	
	public BlockRelay() {
		
		super();
		this.setUnlocalizedName(Names.Blocks.RELAY);
		this.setRegistryName(Names.Blocks.RELAY);
		
	}
	
	@Override
	public void bindTESR() {
		//NO-OP
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new TileRelay();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess worldIn, BlockPos pos, EnumFacing side) {
		return true;
	}

	@Override
	public boolean isBlockNormalCube(IBlockState blockState) {
		return false;
	}

	@Override
	public boolean isOpaqueCube(IBlockState blockState) {
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {

		if (!world.isRemote) {

			if(world.getTileEntity(pos) != null && world.getTileEntity(pos) instanceof TileRelay) {
				TileRelay relay = (TileRelay)world.getTileEntity(pos);

				if(player.isSneaking())
					relay.receivePower(100);

				double energy = relay.getCurrentPower();
				double max = relay.getMaxPower();

				String message = String.format("Energy: %.2ff / %.2ff", energy, max);
				player.sendMessage(new TextComponentString(message));

				relay.markDirty();

			}

		}

		return true;
	}
}
