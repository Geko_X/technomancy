package gekox.electromancy.blocks.relay;

import gekox.electromancy.Electromancy;
import gekox.electromancy.api.power.IElectromancyPowerConsumer;
import gekox.electromancy.blocks.ElectromancyPowerTile;
import net.minecraft.util.math.BlockPos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by Geko_X on 18/10/2016.
 *
 */
public class TileRelay extends ElectromancyPowerTile {
	
	private int searchDist = 16;
	private double powerTransfer = 20;
	
	private List<BlockPos> consumers = new ArrayList<>();

	
	public TileRelay() {
		super();
	}
	
	@Override
	public void update() {
		
		if(!getWorld().isRemote) {
			
			currentTick++;
			
			// For all the power consumers, deposit some energy
			double amount = ((powerTransfer * (storage.getEnergyPercent())) / consumers.size()) + 0.01;
			consumers.forEach((c) -> sendEnergy(c, amount));
			consumers = cleanUp();
			
			if(currentTick >= 20) {
				currentTick = 0;
				
				// First, search out for new power consumers
				search();
				
			}
			
			this.markDirty();
			
		}
		
	}
	
	private void sendEnergy(BlockPos pos, double amount) {

		if(this.getCurrentPower() <= 0)
			return;

		if(getWorld().getTileEntity(pos) != null && getWorld().getTileEntity(pos) instanceof IElectromancyPowerConsumer) {
			
			IElectromancyPowerConsumer c = (IElectromancyPowerConsumer)getWorld().getTileEntity(pos);
			amount = c.receivePower(amount);
			this.extractPower(amount);
			this.markDirty();
			
			// Particles once every 5 ticks
			if(currentTick % 5 == 0) {
				Electromancy.proxy.spawnParticle(this.getWorld(), this.getPos(), pos);
			}
			
		//	LogHelper.info(String.format("Sent %.2f fulmen", amount));
		}
	}
	
	private List<BlockPos> cleanUp() {
		return this.consumers.stream().filter(pos1 -> getWorld().getTileEntity(pos1) != null && getWorld().getTileEntity(pos1) instanceof IElectromancyPowerConsumer).collect(Collectors.toList());
	}
	
	private void search() {
		
		int x1 = this.getPos().getX() - searchDist;
		int y1 = this.getPos().getY() - searchDist;
		int z1 = this.getPos().getZ() - searchDist;
		
		int x2 = this.getPos().getX() + searchDist + 1;
		int y2 = this.getPos().getY() + searchDist + 1;
		int z2 = this.getPos().getZ() + searchDist + 1;
		
		for(int x = x1; x < x2; x++) {
			for(int y = y1; y < y2; y++) {
				for(int z = z1; z < z2; z++) {
					
					BlockPos pos = new BlockPos(x, y, z);
					if(getWorld().getTileEntity(pos) != null && getWorld().getTileEntity(pos) instanceof IElectromancyPowerConsumer && !(getWorld().getTileEntity(pos) instanceof TileRelay)) {
						if(!pos.equals(this.getPos()) && !pos.equals(this.getPos().add(0, -1, 0)))
							consumers.add(pos);
					}
				}
			}
		}
	}
}
