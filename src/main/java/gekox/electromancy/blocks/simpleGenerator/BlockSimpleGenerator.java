package gekox.electromancy.blocks.simpleGenerator;

import gekox.electromancy.Electromancy;
import gekox.electromancy.blocks.ElectromancyBlock;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

/**
 * Created by Geko_X on 2/07/2017.
 *
 */
public class BlockSimpleGenerator extends ElectromancyBlock implements ITileEntityProvider {

	public static final PropertyDirection FACING = PropertyDirection.create("facing");
	public static final PropertyBool ENABLED = PropertyBool.create("enabled");

	public BlockSimpleGenerator() {
		this.setRegistryName(Names.Blocks.SIMPLE_GENERATOR);
		this.setUnlocalizedName(Names.Blocks.SIMPLE_GENERATOR);
		this.setHardness(1.5f);
		this.setHarvestLevel("pickaxe", 2);
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileSimpleGenerator();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {

		if(world.isRemote) {
			return true;
		}

		TileEntity te = world.getTileEntity(pos);
		if(te instanceof TileSimpleGenerator) {
			player.openGui(Electromancy.instance, Reference.GUI.SIMPLE_GENERATOR, world, pos.getX(), pos.getY(), pos.getZ());
			return true;
		}

		return false;

	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack) {
		super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
		worldIn.setBlockState(pos, state.withProperty(FACING, getFacingFromEntity(pos, placer)), 2);
		worldIn.setBlockState(pos, state.withProperty(ENABLED, false));
	}

	public static EnumFacing getFacingFromEntity(BlockPos block, EntityLivingBase entity) {

		return EnumFacing.getFacingFromVector(
				(float) (entity.posX - block.getX()),
				(float) (entity.posY - block.getY()),
				(float) (entity.posZ - block.getZ()));
	}

	@Override
	public int getMetaFromState(IBlockState state) {
		return state.getValue(FACING).getIndex() + (state.getValue(ENABLED) ? 8 : 0);
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, FACING, ENABLED);
	}
}
