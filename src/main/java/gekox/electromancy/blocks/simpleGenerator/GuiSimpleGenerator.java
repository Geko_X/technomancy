package gekox.electromancy.blocks.simpleGenerator;

import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.Color;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Geko_X on 2/07/2017.
 *
 */
public class GuiSimpleGenerator extends GuiContainer {

	public static final int WIDTH = 180;
	public static final int HEIGHT = 152;

	private static final ResourceLocation background = new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, "textures/gui/testcontainer.png");

	private TileSimpleGenerator te;

	public GuiSimpleGenerator(TileSimpleGenerator te, ContainerSimpleGenerator container) {
		super(container);

		this.te = te;

		this.xSize = WIDTH;
		this.ySize = HEIGHT;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		mc.getTextureManager().bindTexture(background);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

		double burnPercent = te.getBurnPercent();
		double totalTime = te.getTotalBurnTime();
		double currentTime = te.getCurrentBurnTime();

		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;

		String str = String.format("%.2f / %.2f (%.2f)", currentTime, totalTime, burnPercent);
		FontRenderer fontRenderer = this.fontRenderer;
		drawString(fontRenderer, str, x, y, Color.MAGIC_COLOR);

	}
}
