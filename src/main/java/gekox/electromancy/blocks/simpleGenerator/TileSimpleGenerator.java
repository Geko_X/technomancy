package gekox.electromancy.blocks.simpleGenerator;

import gekox.electromancy.blocks.ElectromancyTile;
import gekox.electromancy.network.ElectromancyPacketHandler;
import gekox.electromancy.network.MessageUpdateSimpleGenerator;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.world.PotentialEnergyChunk;
import gekox.electromancy.world.PotentialEnergyChunkStore;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

/**
 * Created by Geko_X on 2/07/2017.
 *
 */
public class TileSimpleGenerator extends ElectromancyTile implements ITickable {

	public static final int inventory_size = 1;

	private double energyPerTick = 1;
	private int totalBurnTime;
	private int currentBurnTime;

	private ItemStackHandler itemStackHandler = new ItemStackHandler(inventory_size) {
		@Override
		protected void onContentsChanged(int slot) {
			TileSimpleGenerator.this.markDirty();
		}
	};

	@Override
	public void update() {

		boolean hasWork = this.isBurning();
		boolean markDirty = false;

		if(hasWork) {
			this.currentBurnTime--;
		}

		if(!getWorld().isRemote) {
			if(hasWork) {
				PotentialEnergyChunkStore.addPotential(getWorld(), pos, energyPerTick);
			}

			if(!this.itemStackHandler.getStackInSlot(0).isEmpty()) {

				ItemStack stack = this.itemStackHandler.getStackInSlot(0);

				// Burn the next item
				if(!this.isBurning()) {
					this.totalBurnTime = this.currentBurnTime = TileEntityFurnace.getItemBurnTime(stack);

					if(this.isBurning()) {

						markDirty = true;

						// Use up the item
						if(!stack.isEmpty()) {
							stack.setCount(stack.getCount() - 1);
							itemStackHandler.setStackInSlot(0, stack);
						}
					}
				}
			}

			MessageUpdateSimpleGenerator message = new MessageUpdateSimpleGenerator(this.pos, this.currentBurnTime, this.totalBurnTime);
			ElectromancyPacketHandler.INSTANCE.sendToAll(message);

			// State changed
			if(hasWork != this.isBurning()) {
				getWorld().setBlockState(pos, getWorld().getBlockState(pos).withProperty(BlockSimpleGenerator.ENABLED, this.isBurning()), 3);
			}

		}

		if(markDirty) {
			this.markDirty();
		}
	}

	private void doWork() {
		PotentialEnergyChunkStore.addPotential(this.getWorld(), this.pos, this.energyPerTick);
	}

	public double getBurnPercent() {
		if(totalBurnTime <= 0 || currentBurnTime <= 0)
			return 0;
		return (double)(currentBurnTime) / totalBurnTime;
	}

	public int getTotalBurnTime() {
		return totalBurnTime;
	}

	public int getCurrentBurnTime() {
		return currentBurnTime;
	}

	public boolean isBurning() {
		return this.currentBurnTime > 0;
	}

	public void setTotalBurnTime(int totalBurnTime) {
		this.totalBurnTime = totalBurnTime;
	}

	public void setCurrentBurnTime(int currentBurnTime) {
		this.currentBurnTime = currentBurnTime;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		if (compound.hasKey(Reference.NBT_ITEMS)) {
			itemStackHandler.deserializeNBT((NBTTagCompound) compound.getTag(Reference.NBT_ITEMS));
		}

		if(compound.hasKey(Reference.NBT_TOTAL_WORK))
			this.totalBurnTime = compound.getInteger(Reference.NBT_TOTAL_WORK);

		if(compound.hasKey(Reference.NBT_CURRENT_WORK))
			this.currentBurnTime = compound.getInteger(Reference.NBT_CURRENT_WORK);

	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		compound.setTag(Reference.NBT_ITEMS, itemStackHandler.serializeNBT());
		compound.setInteger(Reference.NBT_TOTAL_WORK, this.totalBurnTime);
		compound.setInteger(Reference.NBT_CURRENT_WORK, this.currentBurnTime);
		return compound;
	}

	public boolean canInteractWith(EntityPlayer playerIn) {
		return !isInvalid() && playerIn.getDistanceSq(pos.add(0.5D, 0.5D, 0.5D)) <= 64;
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return true;
		}
		return super.hasCapability(capability, facing);
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return (T) itemStackHandler;
		}
		return super.getCapability(capability, facing);
	}
}