package gekox.electromancy.blocks.storage;

/**
 * Created by Geko_X on 22/10/2016.
 *
 */

import gekox.electromancy.blocks.ElectromancyPowerTile;
import gekox.electromancy.blocks.relay.TileRelay;
import net.minecraft.util.math.BlockPos;

public class TileStorage extends ElectromancyPowerTile {

	private double powerTransfer = 20;

	public TileStorage() {
		super();
	}

	@Override
	public void update() {

		if(!getWorld().isRemote) {

			BlockPos top = this.getPos().add(0, 1, 0);
			if (getWorld().getTileEntity(top) != null && getWorld().getTileEntity(top) instanceof TileRelay) {
				TileRelay relay = (TileRelay)(getWorld().getTileEntity(top));

				double amount = Math.min(this.getCurrentPower(), powerTransfer);
				this.extractPower(relay.receivePower(amount));

			}

		}
	}
}
