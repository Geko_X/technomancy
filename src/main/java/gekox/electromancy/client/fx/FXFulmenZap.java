package gekox.electromancy.client.fx;

import gekox.electromancy.util.MathsUtils;
import gekox.electromancy.util.Vector3;
import net.minecraft.client.particle.Particle;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Geko_X on 19/10/2016.
 *
 */

public class FXFulmenZap extends Particle {
	
	private Vector3 start;
	private Vector3 end;
	private double speed = 0.06f;
	private BlockPos offset = new BlockPos(0.5, 0.5, 0.5);
	
	public FXFulmenZap(World worldIn, BlockPos start, BlockPos end) {
		
		super(worldIn, start.getX() + 0.5, start.getY() + 0.5, start.getZ() + 0.5, 0, 0, 0);
		
		this.start = new Vector3(start.getX() + 0.5, start.getY() + 0.5, start.getZ() + 0.5);
		this.end = new Vector3(end.getX() + 0.5, end.getY() + 0.5, end.getZ() + 0.5);
		
		this.setParticleTextureIndex(82);
		this.particleScale = 2;
		this.setRBGColorF(0x88, 0x00, 0x88);
		//this.particleMaxAge = (int) Math.floor(speed / Vector3.Distance(this.start, this.end) * 20);
		this.particleGravity = 0;
		
	}
	
	@Override
	public void onUpdate() {
		
		double eps = speed * 1.5;
		if(MathsUtils.almostEqual(posX, end.getX(), eps) &&
				MathsUtils.almostEqual(posY, end.getY(), eps) &&
				MathsUtils.almostEqual(posZ, end.getZ(), eps)) {

			this.setExpired();

		}
		
//		if(particleAge++ >= particleMaxAge)
//			this.setExpired();
		
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		
		Vector3 motion = Vector3.Subtract(end, start);
		motionX = motion.getX() * speed;
		motionY = motion.getY() * speed;
		motionZ = motion.getZ() * speed;
		
		posX += motionX;
		posY += motionY;
		posZ += motionZ;
		
		
	}
}
