package gekox.electromancy.client.render;

import gekox.electromancy.entity.EntityLightning;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.RenderHelper;
import gekox.electromancy.util.Vector3;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
//import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

/**
 * Created by Geko_X on 13/10/2016.
 *
 */
public class RenderLightning extends Render<EntityLightning> {

	private final static ResourceLocation texture = new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, "textures/entities/lightningZap.png");

	public RenderLightning(RenderManager renderManager) {
		super(renderManager);
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityLightning entity) {
		return texture;
	}

	@Override
	public void doRender(EntityLightning ent, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(ent, x, y, z, entityYaw, partialTicks);

		int steps = ent.steps;
		int size = ent.size;

		//LogHelper.info("Steps: " + steps);
		
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder buffer = tessellator.getBuffer();
		
		Minecraft.getMinecraft().renderEngine.bindTexture(getEntityTexture(ent));
		GlStateManager.color(1, 1, 1);
		
		EntityPlayerSP player = Minecraft.getMinecraft().player;
		
		//========================================================================
		
		GlStateManager.depthMask(false);
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_ONE, GL11.GL_ONE);
		
		GlStateManager.pushMatrix();
		
		GlStateManager.translate(x, y + 0.5, z - 0.5);
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
		
		Vector3 start = Vector3.FromBlockPos(ent.getPosition());
		Vector3 end = Vector3.Add(start, new Vector3(0, 5, 0));
		Vector3 middle = Vector3.Add(start, new Vector3(2.5f, 2.5f, 0));
		
		Vector3 playerVector = Vector3.FromBlockPos(player.getPosition());
		playerVector = Vector3.Add(playerVector, new Vector3(0, player.getEyeHeight(), 0));
		
		RenderHelper.drawBeam(start, middle, playerVector, 0.2f);
		RenderHelper.drawBeam(middle, end, playerVector, 0.2f);
		
		tessellator.draw();
		
		//========================================================================
		
//
//		GlStateManager.rotate(ent.prevRotationYaw + (ent.rotationYaw - ent.prevRotationYaw) * partialTicks - 90.0F, 0.0F, 1.0F, 0.0F);
//		GlStateManager.rotate(ent.prevRotationPitch + (ent.rotationPitch - ent.prevRotationPitch) * partialTicks, 0.0F, 0.0F, 1.0F);
//
//		Tessellator tessellator = Tessellator.getInstance();
//		VertexBuffer worldrenderer = tessellator.getBuffer();
//
//		int b = 0;
//		float f = 0.0F;
//		float f1 = 0.5F;
//		float f2 = (float)(0 + b * 10) / 32.0F;
//		float f3 = (float)(5 + b * 10) / 32.0F;
//		float f8 = 0.05625F;
//
//		GlStateManager.disableLighting();
//		GlStateManager.enableBlend();
//		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//		GlStateManager.enableRescaleNormal();
//
//		GlStateManager.scale(f8, f8, f8);
//
//		// The points to draw the lines to
//		double pointsX[] = ent.pointsX;
//		double pointsY[] = ent.pointsY;
//
//		// Each step
//		for(int i = 1; i < steps + 1; i++) {
//
//			double x0 = pointsX[i - 1] / f8;
//			double x1 = pointsX[i] / f8;
//			double y0 = pointsY[i - 1] / f8;
//			double y1 = pointsY[i] / f8;
//
//			GlStateManager.translate(x0, y0, 0);
//
//			double xRot = MathHelper.atan2(x0, x1);
//			double yRot = MathHelper.atan2(y0, y1);
//
//			GlStateManager.rotate((float)xRot, 1.0F, 0.0F, 0.0F);
//			GlStateManager.rotate((float)yRot, 0.0F, 1.0F, 0.0F);
//
//			// Each plane of the step
//			for (int j = 0; j < 4; j++) {
//				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
//				GL11.glNormal3f(0.0F, 0.0F, f8);
//
//				worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX);
//
////				worldrenderer.pos(-8.0D, -1.0D, 0.0D).tex((double) f, (double) f2).endVertex();
////				worldrenderer.pos(8.0D, -1.0D, 0.0D).tex((double) f1, (double) f2).endVertex();
////				worldrenderer.pos(8.0D, 1.0D, 0.0D).tex((double) f1, (double) f3).endVertex();
////				worldrenderer.pos(-8.0D, 1.0D, 0.0D).tex((double) f, (double) f3).endVertex();
//
//				worldrenderer.pos(x0, y0 + 1, 0.0D).tex((double) f, (double) f2).endVertex();
//				worldrenderer.pos(x1, y0 + 1, 0.0D).tex((double) f1, (double) f2).endVertex();
//				worldrenderer.pos(x1, y1 - 1, 0.0D).tex((double) f1, (double) f3).endVertex();
//				worldrenderer.pos(x0, y1 - 1, 0.0D).tex((double) f, (double) f3).endVertex();
//
//				tessellator.draw();
//			}
//
//			GlStateManager.rotate((float)-yRot, 0.0F, 1.0F, 0.0F);
//			GlStateManager.rotate((float)-xRot, 1.0F, 0.0F, 0.0F);
//			GlStateManager.translate(-x1, -y1, 0);
//		}
		
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GlStateManager.popMatrix();

	}
}
