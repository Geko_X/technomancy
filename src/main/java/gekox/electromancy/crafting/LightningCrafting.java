package gekox.electromancy.crafting;

import gekox.electromancy.api.crafting.LightningCraftingRegistry;
import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.blocks.ModBlocks;
import gekox.electromancy.items.ModItems;
import gekox.electromancy.reference.Values;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

/**
 * Created by Geko_X on 23/10/2016.
 *
 */
public class LightningCrafting {

	public static final LightningRecipe voltaicIron = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicIngot, 1, 0),
			Values.Crafting.VOLTAIC_IRON_POTENTIAL,
			Items.IRON_INGOT, Items.REDSTONE);

	public static final LightningRecipe voltaicIronBlock = new LightningRecipe(
			new ItemStack(ModBlocks.blockVoltaicIron),
			Values.Crafting.VOLTAIC_IRON_POTENTIAL * 9,
			Blocks.IRON_BLOCK, Blocks.REDSTONE_BLOCK);

	// Tools
	public static final LightningRecipe voltaicSword = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicSword),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.IRON_SWORD, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe voltaicAxe = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicAxe),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.IRON_AXE, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe voltaicPick = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicPick),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.IRON_PICKAXE, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe voltaicShovel = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicShovel),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.IRON_SHOVEL, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe voltaicHoe = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicHoe),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.IRON_HOE, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe voltaicShears = new LightningRecipe(
			new ItemStack(ModItems.itemVoltaicShears),
			Values.Crafting.VOLTAIC_TOOLS,
			Items.SHEARS, new ItemStack(ModItems.itemVoltaicIngot, 1, 0)
	);

	public static final LightningRecipe longRecipe = new LightningRecipe(
			new ItemStack(Blocks.DIRT),
			75.2,
			Items.COMPASS, Items.APPLE, Blocks.STONE,
			Items.BEEF, Blocks.BOOKSHELF, Blocks.LOG,
			Items.DIAMOND, Items.BED, Items.REEDS
	);

	public static void init() {
		LightningCraftingRegistry.registerRecipe(voltaicIron);
		LightningCraftingRegistry.registerRecipe(voltaicIronBlock);
		LightningCraftingRegistry.registerRecipe(voltaicSword);
		LightningCraftingRegistry.registerRecipe(voltaicPick);
		LightningCraftingRegistry.registerRecipe(voltaicAxe);
		LightningCraftingRegistry.registerRecipe(voltaicShovel);
		LightningCraftingRegistry.registerRecipe(voltaicHoe);
		LightningCraftingRegistry.registerRecipe(voltaicShears);

		LightningCraftingRegistry.registerRecipe(longRecipe);
	}

}
