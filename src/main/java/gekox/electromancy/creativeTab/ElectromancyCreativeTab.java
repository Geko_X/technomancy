package gekox.electromancy.creativeTab;

import gekox.electromancy.items.ModItems;
import gekox.electromancy.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

/**
 * Created by Geko_X on 13/10/2016.
 *
 */
public class ElectromancyCreativeTab {

	public static CreativeTabs ELECTROMANCY = new CreativeTabs(Reference.RESOURCE_PREFIX_NO_COLON) {

		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(ModItems.itemVoltaicIngot, 1, 0);
		}
	};

}
