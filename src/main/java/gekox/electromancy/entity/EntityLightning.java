package gekox.electromancy.entity;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * Created by Geko_X on 13/10/2016.
 *
 */
public class EntityLightning extends Entity {

	private int lifetime = 100;
	protected NBTTagCompound customData = new NBTTagCompound();

	public int steps = 5;
	public int size = 10;

	public double pointsX[] = new double[steps + 1];
	public double pointsY[] = new double[steps + 1];

	public EntityLightning(World world) {
		super(world);
		this.setSize(1, 1);
		this.setup();
	}

	public EntityLightning(World world, double x, double y, double z) {
		super(world);
		this.setLocationAndAngles(x, y, z, 0, 0);
		this.setSize(1, 1);
		this.setup();
	}

	private void setup() {

		customData.setInteger("steps", this.steps);
		customData.setInteger("size", this.size);
		customData.setInteger("lifetime", this.lifetime);

		for(int i = 0; i < steps + 1; i++) {
			pointsX[i] = i * (size / steps) + (this.rand.nextDouble() - 0.5);
			pointsY[i] = (this.rand.nextDouble() - 0.5);
		}

		pointsX[0] = 0;
		pointsX[steps] = size;

		pointsY[0] = 0;
		pointsY[steps] = 0;

	}

	@Override
	protected void entityInit() {

	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound nbt) {
		this.lifetime = nbt.getInteger("lifetime");
		this.size = nbt.getInteger("size");
		this.steps = nbt.getInteger("steps");
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound nbt) {
		nbt.setInteger("lifetime", lifetime);
		nbt.setTag("customDataBase", customData);
	}

	@Override
	public void onUpdate() {
		super.onUpdate();

		lifetime--;
		if(lifetime <= 0)
			this.setDead();

	}
}
