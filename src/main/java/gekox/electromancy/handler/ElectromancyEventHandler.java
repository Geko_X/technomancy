package gekox.electromancy.handler;

import gekox.electromancy.api.crafting.LightningCraftingRegistry;
import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.reference.Values;
import gekox.electromancy.world.PotentialEnergyChunk;
import gekox.electromancy.world.PotentialEnergyChunkStore;
import net.minecraft.block.Block;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Geko_X on 12/10/2016.
 *
 */
public class ElectromancyEventHandler {

	private final HashMap<Integer, Integer> serverTicks = new HashMap<>();

	@SubscribeEvent
	public void onServerWorldTick(TickEvent.WorldTickEvent event) {

		if (event.world.isRemote)
			return;

		int dim = event.world.provider.getDimension();
		if (event.phase == TickEvent.Phase.START) {
			if (!this.serverTicks.containsKey(dim))
				this.serverTicks.put(dim, 0);

			int ticks = (this.serverTicks.get(dim));

			if (ticks % 20 == 0) {

				//LogHelper.info("Server tick");

				// Dirty chunks
				processAllChunks(event);
				processDirtyChunks(event);

			}
			this.serverTicks.put(dim, ticks + 1);
		}
	}

	private void processAllChunks(TickEvent.WorldTickEvent event) {
		int dim = event.world.provider.getDimension();
		World world = event.world;
		ConcurrentHashMap<ChunkPos, PotentialEnergyChunk> chunks = PotentialEnergyChunkStore.getAllChunksForWorld(dim);

		for (PotentialEnergyChunk pChunk : chunks.values()) {
			double potential = pChunk.getPotential();

			// I have no idea. IntelliJ does, however...
			LightningCraftingRegistry.getRecipies().stream().filter(recipe -> potential > recipe.getPotential()).forEach(recipe -> {
				doLightningCrafting(event, pChunk, recipe);
			});
		}
	}

	private void doLightningCrafting(TickEvent.WorldTickEvent event, PotentialEnergyChunk pChunk, LightningRecipe recipe) {

		//LogHelper.info("Lighting recipe!");

		World world = event.world;
		Chunk chunk = world.getChunkFromChunkCoords(pChunk.getPos().x, pChunk.getPos().z);

		AxisAlignedBB aabb = new AxisAlignedBB(chunk.x << 4, 0, chunk.z << 4, (chunk.x << 4) + 15, 128, (chunk.z << 4) + 15);
		List<EntityItem> items = new ArrayList<>();
		chunk.getEntitiesOfTypeWithinAABB(EntityItem.class, aabb, items, null);

		List<ItemStack> inputs = new ArrayList<>();

		for (EntityItem item : items) {
			ItemStack stack1 = item.getItem();
			ItemStack targetStack = recipe.getRecipeInput().get(0);

			if (stack1.getItem() == targetStack.getItem() && stack1.getCount() >= targetStack.getCount()) {

				BlockPos pos = item.getPosition();
				AxisAlignedBB aabb2 = new AxisAlignedBB(pos.getX() - 1, pos.getY(), pos.getZ() - 1, pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1);

				List<EntityItem> items2 = world.getEntitiesWithinAABB(EntityItem.class, aabb2);

				for (EntityItem ent : items2) {
					inputs.add(ent.getItem());
				}

				if (recipe.matches(inputs, pChunk.getPotential())) {

					// Reduce the number of items in the dropped stacks
					for (EntityItem ent : items2) {
						ItemStack stack = ent.getItem();
						stack.setCount(stack.getCount() - 1);

						if (stack.getCount() > 0)
							ent.setItem(stack);
						else
							ent.setDead();
					}

					ItemStack result = recipe.getRecipeOutput();
					EntityItem output = new EntityItem(world, pos.getX(), pos.getY() + 1, pos.getZ(), result);
					world.spawnEntity(output);

					world.addWeatherEffect(new EntityLightningBolt(world, pos.getX(), pos.getY(), pos.getZ(), true));
					PotentialEnergyChunkStore.removePotential(world.provider.getDimension(), pChunk.getPos().x, pChunk.getPos().z, recipe.getPotential());

					break;

				}
			}
		}
	}

	private void processDirtyChunks(TickEvent.WorldTickEvent event) {
		int dim = event.world.provider.getDimension();
		CopyOnWriteArrayList<ChunkPos> dirtyChunks = PotentialEnergyChunkStore.dirtyChunks.get(dim);

		if ((dirtyChunks != null) && (dirtyChunks.size() > 0)) {
			for (ChunkPos pos : dirtyChunks) {

				//LogHelper.info(String.format("Chunk(%d, %d)", pos.chunkXPos, pos.chunkZPos));

				PotentialEnergyChunkStore.processPotentialChunk(dim, pos);
				event.world.markChunkDirty(new BlockPos(pos.x, 1, pos.z), null);
			}

			dirtyChunks.clear();
		}
	}

	@SubscribeEvent
	public void chunkSaveEvent(ChunkDataEvent.Save event) {


	//	LogHelper.info("Chunk save event");

		int dim = event.getWorld().provider.getDimension();
		ChunkPos pos = event.getChunk().getPos();

		NBTTagCompound nbt = new NBTTagCompound();

		PotentialEnergyChunk pchunk = PotentialEnergyChunkStore.getPotentialChunkForWorld(dim, pos.x, pos.z);
		if (pchunk != null) {

			nbt.setShort("base", (short)1);
			nbt.setDouble("potential", pchunk.getPotential());

//			if (!event.getChunk().isLoaded())
//				WorldPotentialHandler.removePotentialChunk(dim, pos.chunkXPos, pos.chunkZPos);
		}

		event.getData().setTag("Electromancy", nbt);

	}

	@SubscribeEvent
	public void chunkLoadEvent(ChunkDataEvent.Load event) {

//		if(event.getWorld().isRemote)
//			return;

	//	LogHelper.info("Chunk load event");

		int dim = event.getWorld().provider.getDimension();

		if(event.getData().getCompoundTag("Electromancy").hasKey("base")) {

			NBTTagCompound nbt = event.getData().getCompoundTag("Electromancy");
			short base = nbt.getShort("base");
			double potential = nbt.getDouble("potential");
			PotentialEnergyChunkStore.addChunk(dim, event.getChunk().getPos(), potential);
		}
	}

	@SubscribeEvent
	public void explodeStartEvent(ExplosionEvent.Start event) {

		Explosion explosion = event.getExplosion();
		World world = event.getWorld();
		BlockPos pos = new BlockPos(explosion.getPosition()).add(0, -1, 0);

		Block block = world.getBlockState(pos).getBlock();
		if(block == Blocks.GOLD_BLOCK) {
			event.setCanceled(true);
			explosion.doExplosionB(true);

			PotentialEnergyChunkStore.addPotential(world, pos, Values.Generating.EXPLOSION);
			// Todo: maybe spawn some particles?

		}
	}

}
