package gekox.electromancy.integration.jei;

import gekox.electromancy.api.crafting.LightningCraftingRegistry;
import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.blocks.ModBlocks;
import gekox.electromancy.integration.jei.lightning.LightningCategory;
import gekox.electromancy.integration.jei.lightning.LightningWrapperFactory;
import gekox.electromancy.items.ModItems;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import mezz.jei.api.*;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Geko_X on 22/06/2017.
 *
 */

@mezz.jei.api.JEIPlugin
public class ElectromancyPlugin implements IModPlugin {

	private final String lightningUID = Reference.JEI_LIGHTNING;

	@Override
	public void register(@Nonnull IModRegistry registry) {

		LogHelper.info("Registering JEI");

		IJeiHelpers jeiHelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();

		// Descriptions
		addJEIDescription(registry);

		// Categories
		//registry.addRecipeCategories(new LightningCategory(guiHelper));

		// Handlers
		registry.handleRecipes(LightningRecipe.class, new LightningWrapperFactory(), lightningUID);

		// Recipes
		registry.addRecipes(LightningCraftingRegistry.getRecipies(), lightningUID);

		// Crafting Items
		registry.addRecipeCatalyst(new ItemStack(ModBlocks.blockVoltaicIron), lightningUID);
	}

	@Override
	public void registerCategories(IRecipeCategoryRegistration registry) {

		IJeiHelpers jeiHelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();

		registry.addRecipeCategories(new LightningCategory(guiHelper));
	}

	public void addJEIDescription(IModRegistry registry) {

		List<ItemStack> voltaicIron = new ArrayList<>();
		voltaicIron.add(new ItemStack(ModItems.itemVoltaicIngot, 1, 0));
		voltaicIron.add(new ItemStack(ModItems.itemVoltaicNugget, 1, 0));
		voltaicIron.add(new ItemStack(ModBlocks.blockVoltaicIron, 1, 0));
		registry.addIngredientInfo(voltaicIron, ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_iron");

		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicSword), ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_sword");
		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicPick), ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_pick");
		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicAxe), ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_axe");
		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicShovel), ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_shovel");
		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicHoe), ItemStack.class, Reference.JEI_DESCRIPTION + "voltaic_hoe");
		registry.addIngredientInfo(new ItemStack(ModItems.itemVoltaicShears), ItemStack.class,Reference.JEI_DESCRIPTION + "voltaic_shears");


	}
}
