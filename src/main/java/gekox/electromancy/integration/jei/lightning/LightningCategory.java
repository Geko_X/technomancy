package gekox.electromancy.integration.jei.lightning;

import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.TextUtils;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

/**
 * Created by Geko_X on 22/06/2017.
 *
 */

public class LightningCategory implements IRecipeCategory<LightningWrapper> {

	private LightningRecipe recipe;

	private IDrawable background;

	private final int outputSlot = 0;
	private final int inputSlot = 1;

	public LightningCategory(IGuiHelper helper) {
		ResourceLocation location = new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, "textures/gui/jei_lightning.png");
		background = helper.createDrawable(location, 0, 0, 116, 54);
	}

	@Override
	public String getUid() {
		return Reference.JEI_LIGHTNING;
	}

	@Override
	public String getTitle() {
		return TextUtils.localize("jei.electromancy.lightning");
	}

	@Override
	public String getModName() {
		return Reference.NAME;
	}

	@Override
	public IDrawable getBackground() {
		return background;
	}


	@Override
	public void setRecipe(@Nonnull IRecipeLayout recipeLayout, @Nonnull LightningWrapper recipeWrapper, @Nonnull IIngredients ingredients) {

		int x = 116;
		int y = 54;
		int i = 18;

		LightningWrapper wrapper = (LightningWrapper)recipeWrapper;
		recipe = wrapper.getRecipe();

		IGuiItemStackGroup stackGroup = recipeLayout.getItemStacks();

		int inputX = ((x - i) / 2) - (i * 3) - (i / 2);
		int inputY = ((y - i) / 2) - i;

		for (int k = 0; k < 3; ++k) {
			for (int j = 0; j < 3; ++j) {
				int index = inputSlot + j + (k * 3);
				stackGroup.init(index, true, inputX + (j * i), inputY + (k * i));
			}
		}

		for(int j = 0; j < recipe.getRecipeInput().size(); j++) {
			stackGroup.set(inputSlot + j, recipe.getRecipeInput().get(j));
		}

		int outputY = (y - i) / 2;
		int outputX = x - (x / 4) - (i / 2);

		stackGroup.init(outputSlot, false, outputX, outputY);
		stackGroup.set(outputSlot, recipe.getRecipeOutput());
	}
}
