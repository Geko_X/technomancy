package gekox.electromancy.integration.jei.lightning;

import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.reference.Reference;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

/**
 * Created by Geko_X on 22/06/2017.
 *
 */

public class LightningHandler implements IRecipeHandler<LightningRecipe> {
	@Override
	public Class<LightningRecipe> getRecipeClass() {
		return LightningRecipe.class;
	}

	@Override
	public String getRecipeCategoryUid(LightningRecipe recipe) {
		return Reference.JEI_LIGHTNING;
	}

	@Override
	public IRecipeWrapper getRecipeWrapper(LightningRecipe recipe) {
		return new LightningWrapper(recipe);
	}

	@Override
	public boolean isRecipeValid(LightningRecipe recipe) {
		//TODO: Actually check if valid
		return true;
	}
}
