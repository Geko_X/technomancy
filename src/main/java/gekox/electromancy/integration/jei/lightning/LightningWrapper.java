package gekox.electromancy.integration.jei.lightning;

import gekox.electromancy.api.crafting.LightningRecipe;
import gekox.electromancy.util.Color;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.BlankRecipeWrapper;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Geko_X on 22/06/2017.
 *
 */

public class LightningWrapper implements IRecipeWrapper {

	private List<ItemStack> inputs;
	private double power;
	private ItemStack output;

	private LightningRecipe recipe;

	public LightningWrapper(LightningRecipe recipe) {
		this.inputs = recipe.getRecipeInput();
		this.power = recipe.getPotential();
		this.output = recipe.getRecipeOutput();
		this.recipe = recipe;
	}

	@Override
	public void getIngredients(@Nonnull IIngredients ingredients) {
		ingredients.setInputs(ItemStack.class, inputs);
		ingredients.setOutput(ItemStack.class, output);
	}

	public LightningRecipe getRecipe() {
		return recipe;
	}

	@Override
	public void drawInfo(@Nonnull Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {

		int x = 116;
		int y = 54;

		FontRenderer fontRenderer = minecraft.fontRenderer;
		String energyText = "" + recipe.getPotential();
		int length = fontRenderer.getStringWidth(energyText);
		int start = (x - length) / 2;

		fontRenderer.drawString(energyText, start, y - 7, Color.MAGIC_COLOR);
	}

	@Nullable
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY) {
		return null;
	}

	@Override
	public boolean handleClick(@Nonnull Minecraft minecraft, int mouseX, int mouseY, int mouseButton) {
		return false;
	}
}
