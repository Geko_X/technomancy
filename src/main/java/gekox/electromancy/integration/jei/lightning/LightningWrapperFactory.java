package gekox.electromancy.integration.jei.lightning;

import gekox.electromancy.api.crafting.LightningRecipe;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.api.recipe.IRecipeWrapperFactory;

/**
 * Created by Geko_X on 2/07/2017.
 *
 */
public class LightningWrapperFactory implements IRecipeWrapperFactory<LightningRecipe> {
	@Override
	public IRecipeWrapper getRecipeWrapper(LightningRecipe recipe) {
		return new LightningWrapper(recipe);
	}
}
