package gekox.electromancy.integration.tcon;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;

/**
 * Created by Geko_X on 28/06/2017.
 *
 */
public class FluidVoltaicIron extends Fluid {

	private static final ResourceLocation still = new ResourceLocation("tconstruct:blocks/fluids/molten_metal");
	private static final ResourceLocation flowing = new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow");

	private static final String name = "fluid_voltaic_iron";

	private int color;

	public FluidVoltaicIron(int color) {
		super(name, still, flowing);
		this.color = color;
		this.setTemperature(1000);
		this.setViscosity(250);
	}

	@Override
	public int getColor() {
		return this.color;
	}

	@Override
	public ResourceLocation getStill() {
		return still;
	}

	@Override
	public ResourceLocation getFlowing() {
		return flowing;
	}

}
