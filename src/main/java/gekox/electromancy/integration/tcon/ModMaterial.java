package gekox.electromancy.integration.tcon;

import gekox.electromancy.blocks.ModBlocks;
import gekox.electromancy.items.ModItems;
import gekox.electromancy.reference.Names;
import gekox.electromancy.util.LogHelper;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

//import net.minecraftforge.fml.common.event.FMLInterModComms;
//import net.minecraftforge.fml.common.registry.GameRegistry;
//import slimeknights.tconstruct.library.TinkerRegistry;
//import slimeknights.tconstruct.library.materials.ExtraMaterialStats;
//import slimeknights.tconstruct.library.materials.HandleMaterialStats;
//import slimeknights.tconstruct.library.materials.HeadMaterialStats;
//import slimeknights.tconstruct.library.materials.Material;
//import slimeknights.tconstruct.tools.traits.TraitShocking;

//import static slimeknights.tconstruct.library.utils.HarvestLevels.DIAMOND;

/**
 * Created by Geko_X on 28/06/2017.
 *
 */
public class ModMaterial {
/*
	public static int colorVoltaicIron = 0x6398d8;

	public static Material materialVoltaicIron;
	//public static Fluid fluidVoltaicIron;
	//public static Block fluidBlockVoltaicIron;

	public static void registerMaterials() {

		LogHelper.info("Tinkering away...");

		if(!FluidRegistry.isUniversalBucketEnabled())
			FluidRegistry.enableUniversalBucket();

//		materialVoltaicIron = TConHelper.registerIngotMaterial("voltaic_iron", "VoltaicIron", colorVoltaicIron, 1500);
//		materialVoltaicIron.addTrait(new TraitShocking());
//		materialVoltaicIron.setCastable(true);
//
//		TinkerRegistry.registerMelting(new ItemStack(ModItems.itemVoltaicIngot, 1, 0), FluidRegistry.getFluid("voltaic_iron"), Material.VALUE_Ingot);
//		TinkerRegistry.registerMelting(new ItemStack(ModItems.itemVoltaicIngot, 1, 1), FluidRegistry.getFluid("voltaic_iron"), Material.VALUE_Nugget);
//		TinkerRegistry.registerMelting(new ItemStack(ModBlocks.blockVoltaicIron, 1, 0), FluidRegistry.getFluid("voltaic_iron"), Material.VALUE_Block);
//
//		TinkerRegistry.addMaterialStats(materialVoltaicIron,
//				new HeadMaterialStats(204, 6.00f, 4.00f, DIAMOND),
//				new HandleMaterialStats(0.85f, 60),
//				new ExtraMaterialStats(50));

		// Tool material
		materialVoltaicIron = new Material("electromancy.voltaic_iron", colorVoltaicIron);
		materialVoltaicIron.addCommonItems("VoltaicIron");
		materialVoltaicIron.setCastable(true);
		materialVoltaicIron.setRepresentativeItem(ModBlocks.blockVoltaicIron);
		materialVoltaicIron.addTrait(new TraitShocking());
		TinkerRegistry.integrate(materialVoltaicIron).integrate();

		TinkerRegistry.addMaterialStats(materialVoltaicIron,
				new HeadMaterialStats(204, 6.00f, 4.00f, DIAMOND),
				new HandleMaterialStats(0.85f, 60),
				new ExtraMaterialStats(50));

		// Material fluid
//		Fluid fluidVoltaicIron = new FluidVoltaicIron(colorVoltaicIron);
//		FluidRegistry.registerFluid(fluidVoltaicIron);
//		FluidRegistry.addBucketForFluid(fluidVoltaicIron);
//
//		// Fluid block
//		Block fluidBlockVoltaicIron = new BlockFluidClassic(fluidVoltaicIron, net.minecraft.block.material.Material.LAVA);
//		fluidBlockVoltaicIron.setRegistryName(Names.Blocks.VOLTAIC_IRON_BLOCK + "_fluid");
//		GameRegistry.register(fluidBlockVoltaicIron);
//		GameRegistry.register(new ItemBlock(fluidBlockVoltaicIron), fluidBlockVoltaicIron.getRegistryName());

		TConHelper.registerFluid("voltaic_iron", colorVoltaicIron, 1000);

		// IMC things
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setString("fluid", FluidRegistry.getFluid("voltaic_iron").getName());
		nbt.setString("ore", "VoltaicIron");
		nbt.setBoolean("toolforge", true);

		FMLInterModComms.sendMessage("tconstruct", "integrateSmeltery", nbt);

		LogHelper.info("Created something shocking!");

	}
	*/
}
