package gekox.electromancy.items;


import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Reference;
import net.minecraft.item.Item;

public class ElectromancyItem extends Item {

	public ElectromancyItem() {
		super();
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);
	}

	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

}
