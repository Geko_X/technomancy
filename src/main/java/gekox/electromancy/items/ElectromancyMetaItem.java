package gekox.electromancy.items;

import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public class ElectromancyMetaItem extends ElectromancyItem {
	
	private int numOfItems;
	public String[] names = {
			"0",
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"10",
			"11",
			"12",
			"13",
			"14",
			"15"
	};
	
	public ElectromancyMetaItem(int items) {
		this.numOfItems = items;
		this.hasSubtypes = true;
	}
	
	public ElectromancyMetaItem() {
		this(16);
	}
	
	public String getUnlocalizedName(ItemStack stack) {
		return String.format("item.%s_%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[stack.getItemDamage()]);
	}
	
	public String getUnlocalizedName(int meta) {
		return String.format("item.%s_%s", getUnwrappedUnlocalizedName(super.getUnlocalizedName()), names[meta]);
	}
	
	@Override
	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}
	
	public int getNumOfItems() {
		return numOfItems;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
		if(isInCreativeTab(tab)) {
			for (int i = 0; i < numOfItems; i++) {
				items.add(new ItemStack(this, 1, i));
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel() {
		for(int i = 0; i < this.getNumOfItems(); i++) {
			ModelResourceLocation resourceLocation = new ModelResourceLocation(getRegistryName() + "_" + this.names[i]);
			ModelLoader.setCustomModelResourceLocation(this, i, resourceLocation);
		}
	}
}
