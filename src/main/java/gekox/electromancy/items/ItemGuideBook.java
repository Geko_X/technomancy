package gekox.electromancy.items;

import gekox.electromancy.reference.Names;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 4/10/2016.
 *
 */
public class ItemGuideBook extends ElectromancyItem {

	public ItemGuideBook() {
		super();
		this.setUnlocalizedName(Names.Items.GUIDE_BOOK);
		this.setRegistryName(Names.Items.GUIDE_BOOK);
	}
}
