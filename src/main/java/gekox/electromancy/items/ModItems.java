package gekox.electromancy.items;

import gekox.electromancy.items.gear.ItemLightning;
import gekox.electromancy.items.gear.ItemPotentiometer;
import gekox.electromancy.items.gear.ItemPowerStorage;
import gekox.electromancy.items.material.ItemVoltaicIngot;
import gekox.electromancy.items.material.ItemVoltaicNugget;
import gekox.electromancy.items.tools.*;
import gekox.electromancy.util.LogHelper;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

/**
 * Created by Geko_X on 4/10/2016.
 *
 */
public class ModItems {

	//public static Item itemGuideBook;
	@GameRegistry.ObjectHolder("electromancy:power_storage")
	public static ItemPowerStorage itemPowerStorage;
	@GameRegistry.ObjectHolder("electromancy:potentiometer")
	public static ItemPotentiometer itemPotentiometer;
	@GameRegistry.ObjectHolder("electromancy:lightning")
	public static ItemLightning itemLightning;

	@GameRegistry.ObjectHolder("electromancy:voltaic_iron_ingot")
	public static ItemVoltaicIngot itemVoltaicIngot;
	@GameRegistry.ObjectHolder("electromancy:voltaic_iron_nugget")
	public static ItemVoltaicNugget itemVoltaicNugget;

	@GameRegistry.ObjectHolder("electromancy:voltaic_sword")
	public static ItemVoltaicSword itemVoltaicSword;
	@GameRegistry.ObjectHolder("electromancy:voltaic_pick")
	public static ItemVoltaicPick itemVoltaicPick;
	@GameRegistry.ObjectHolder("electromancy:voltaic_axe")
	public static ItemVoltaicAxe itemVoltaicAxe;
	@GameRegistry.ObjectHolder("electromancy:voltaic_shovel")
	public static ItemVoltaicShovel itemVoltaicShovel;
	@GameRegistry.ObjectHolder("electromancy:voltaic_hoe")
	public static ItemVoltaicHoe itemVoltaicHoe;
	@GameRegistry.ObjectHolder("electromancy:voltaic_shears")
	public static ItemVoltaicShears itemVoltaicShears;

	public static void init() {
		
	}

	@SideOnly(Side.CLIENT)
	public static void initRenders() {

		initModel(itemVoltaicIngot);
		initModel(itemVoltaicNugget);

		initModel(itemLightning);
		initModel(itemPotentiometer);
		initModel(itemPowerStorage);

		initModel(itemVoltaicSword);
		initModel(itemVoltaicPick);
		initModel(itemVoltaicAxe);
		initModel(itemVoltaicShovel);
		initModel(itemVoltaicHoe);
		initModel(itemVoltaicShears);

	}

	public static void registerItem(RegistryEvent.Register<Item> event) {

		event.getRegistry().register(new ItemVoltaicIngot());
		event.getRegistry().register(new ItemVoltaicNugget());

		event.getRegistry().register(new ItemLightning());
		event.getRegistry().register(new ItemPotentiometer());
		event.getRegistry().register(new ItemPowerStorage());

		event.getRegistry().register(new ItemVoltaicSword());
		event.getRegistry().register(new ItemVoltaicPick());
		event.getRegistry().register(new ItemVoltaicAxe());
		event.getRegistry().register(new ItemVoltaicShovel());
		event.getRegistry().register(new ItemVoltaicHoe());
		event.getRegistry().register(new ItemVoltaicShears());

	}

	@SideOnly(Side.CLIENT)
	public static void initModel(Item item) {
		LogHelper.info("Registering model for " + item.toString());
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}
}
