package gekox.electromancy.items.gear;

import gekox.electromancy.entity.EntityLightning;
import gekox.electromancy.items.ElectromancyItem;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.world.PotentialEnergyChunkStore;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Geko_X on 13/10/2016.
 *
 */
public class ItemLightning extends ElectromancyItem {
	
	public ItemLightning() {
		
		super();
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.LIGHTNING);
		this.setRegistryName(Names.Items.LIGHTNING);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
		
		if (hand == EnumHand.MAIN_HAND) {
			if (!world.isRemote) {
				EntityLightning lightning = new EntityLightning(world, player.posX, player.posY, player.posZ);
				world.spawnEntity(lightning);
				
				PotentialEnergyChunkStore.addPotential(world, new BlockPos(player.posX, player.posY, player.posZ), 10D);
			}
		}
		
		return new ActionResult<>(EnumActionResult.SUCCESS, player.getHeldItem(hand));
		
	}
}

