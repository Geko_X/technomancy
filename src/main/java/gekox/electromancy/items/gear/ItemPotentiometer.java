package gekox.electromancy.items.gear;

import gekox.electromancy.items.ElectromancyItem;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import gekox.electromancy.world.PotentialEnergyChunkStore;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 12/10/2016.
 *
 */
public class ItemPotentiometer extends ElectromancyItem {

	public ItemPotentiometer() {
		super();
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.POTENTIOMETER);
		this.setRegistryName(Names.Items.POTENTIOMETER);

		this.setMaxStackSize(1);

	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {

		if(!world.isRemote) {

			BlockPos pos = new BlockPos(player.posX, player.posY, player.posZ);
			double potentialEnergy = PotentialEnergyChunkStore.getPotential(world, pos);

			String str = String.format("Potential energy at chunk(%d, %d): %f", pos.getX() >> 4, pos.getZ() >> 4, potentialEnergy);
			LogHelper.info(str);
			player.sendMessage(new TextComponentString(str));
		}

		return new ActionResult<>(EnumActionResult.SUCCESS, player.getHeldItem(hand));

	}
}
