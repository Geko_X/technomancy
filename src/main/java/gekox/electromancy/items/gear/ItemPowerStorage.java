package gekox.electromancy.items.gear;

import gekox.electromancy.api.power.ElectromancyPowerStorage;
import gekox.electromancy.api.power.IElectromancyPowerStorage;
import gekox.electromancy.items.ElectromancyItem;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 10/10/2016.
 *
 * PowerStorage test
 *
 */
public class ItemPowerStorage extends ElectromancyItem implements IElectromancyPowerStorage {

	private ElectromancyPowerStorage powerStorage = new ElectromancyPowerStorage();

	public ItemPowerStorage() {
		super();
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.POWER_STORAGE);
		this.setRegistryName(Names.Items.POWER_STORAGE);
	}

	@Override
	public double getCurrentPower() {
		return powerStorage.getCurrentPower();
	}

	@Override
	public void setCurrentPower(double power) {
		powerStorage.setCurrentPower(power);
	}

	@Override
	public double getMaxPower() {
		return powerStorage.getMaxPower();
	}

	@Override
	public void setMaxPower(double power) {
		powerStorage.setMaxPower(power);
	}

	@Override
	public double extractPower(double power) {
		return powerStorage.extractPower(power);
	}

	@Override
	public double receivePower(double power) {
		return powerStorage.receivePower(power);
	}

	public boolean showDurabilityBar(ItemStack stack) {
		return true;
	}

	public double getDurabilityForDisplay(ItemStack stack) {
		return (double)this.getCurrentPower() / (double)this.getMaxPower();
	}

}
