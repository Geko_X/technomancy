package gekox.electromancy.items.material;

import gekox.electromancy.items.ElectromancyItem;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public class ItemVoltaicIngot extends ElectromancyItem {
	
	public ItemVoltaicIngot() {

		super();

		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_IRON_INGOT);
		this.setRegistryName(Names.Items.VOLTAIC_IRON_INGOT);
	}
}
