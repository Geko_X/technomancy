package gekox.electromancy.items.material;

import gekox.electromancy.items.ElectromancyItem;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public class ItemVoltaicNugget extends ElectromancyItem {

	public ItemVoltaicNugget() {

		super();

		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_IRON_NUGGET);
		this.setRegistryName(Names.Items.VOLTAIC_IRON_NUGGET);
	}
	
}
