package gekox.electromancy.items.tools;

import com.google.common.collect.Sets;
import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.reference.Values;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.*;
import net.minecraftforge.fml.common.registry.GameRegistry;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * Created by Geko_X on 21/06/2017.
 *
 */
public class ItemVoltaicAxe extends ItemTool {

	private static final Set<Block> EFFECTIVE_ON = Sets.newHashSet(new Block[] {Blocks.PLANKS, Blocks.BOOKSHELF, Blocks.LOG, Blocks.LOG2, Blocks.CHEST, Blocks.PUMPKIN, Blocks.LIT_PUMPKIN, Blocks.MELON_BLOCK, Blocks.LADDER, Blocks.WOODEN_BUTTON, Blocks.WOODEN_PRESSURE_PLATE});

	public ItemVoltaicAxe() {
		super(Values.Tools.VOLTAIC_IRON, EFFECTIVE_ON);
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_AXE);
		this.setRegistryName(Names.Items.VOLTAIC_AXE);
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);

		// Axe specific things. Why Mojang, WHYYYY??
		this.damageVsEntity = Values.Tools.VOLTAIC_IRON.getDamageVsEntity() + 4;
		this.attackSpeed = -3.1f;
	}

	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

	@Override
	public float getStrVsBlock(@Nonnull ItemStack stack, IBlockState state) {
		Material material = state.getMaterial();
		return material != Material.WOOD && material != Material.PLANTS && material != Material.VINE ? super.getStrVsBlock(stack, state) : this.efficiencyOnProperMaterial;
	}
}
