package gekox.electromancy.items.tools;

import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.reference.Values;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 21/06/2017.
 *
 */
public class ItemVoltaicPick extends ItemPickaxe {

	public ItemVoltaicPick() {
		super(Values.Tools.VOLTAIC_IRON);
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_PICK);
		this.setRegistryName(Names.Items.VOLTAIC_PICK);
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);
	}

	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

}
