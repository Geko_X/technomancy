package gekox.electromancy.items.tools;

import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import net.minecraft.item.ItemShears;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 21/06/2017.
 *
 */
public class ItemVoltaicShears extends ItemShears {

	public ItemVoltaicShears() {
		super();
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_SHEARS);
		this.setRegistryName(Names.Items.VOLTAIC_SHEARS);
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);
		this.setMaxDamage(300);
	}

	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

}
