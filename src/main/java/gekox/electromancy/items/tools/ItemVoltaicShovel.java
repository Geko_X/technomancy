package gekox.electromancy.items.tools;

import gekox.electromancy.creativeTab.ElectromancyCreativeTab;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.reference.Values;
import net.minecraft.item.ItemSpade;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Geko_X on 21/06/2017.
 *
 */
public class ItemVoltaicShovel extends ItemSpade {

	public ItemVoltaicShovel() {
		super(Values.Tools.VOLTAIC_IRON);
		this.setUnlocalizedName(Reference.RESOURCE_PREFIX + Names.Items.VOLTAIC_SHOVEL);
		this.setRegistryName(Names.Items.VOLTAIC_SHOVEL);
		this.setCreativeTab(ElectromancyCreativeTab.ELECTROMANCY);
	}

	@Override
	public String getUnlocalizedName() {
		return String.format("item.%s%s", Reference.RESOURCE_PREFIX, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String unlocalizedName) {
		return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
	}

}
