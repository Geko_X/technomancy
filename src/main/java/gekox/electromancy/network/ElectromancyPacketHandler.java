package gekox.electromancy.network;

import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Created by Geko_X on 3/07/2017.
 *
 */
public class ElectromancyPacketHandler {

	public static final SimpleNetworkWrapper INSTANCE = new SimpleNetworkWrapper(Reference.MODID);

	private static int id = -1;

	public static int nextID() {
		return id++;
	}

	public static void init() {

		LogHelper.info("Registering network handler");

		// Server -> Client
		INSTANCE.registerMessage(MessageUpdateSimpleGenerator.class, MessageUpdateSimpleGenerator.class, nextID(), Side.CLIENT);

		// Client -> Server

		LogHelper.info("Done!");

	}

	public static void sendToAllAround(IMessage message, TileEntity te, int range) {
		INSTANCE.sendToAllAround(message, new NetworkRegistry.TargetPoint(te.getWorld().provider.getDimension(), te.getPos().getX(), te.getPos().getY(), te.getPos().getZ(), range));
	}

	public static void sendToAllAround(IMessage message, TileEntity te) {
		sendToAllAround(message, te, 64);
	}

	public static void sendTo(IMessage message, EntityPlayerMP player) {
		INSTANCE.sendTo(message, player);
	}

}
