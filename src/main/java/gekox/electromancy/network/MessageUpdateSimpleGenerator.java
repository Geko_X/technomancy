package gekox.electromancy.network;

import gekox.electromancy.blocks.simpleGenerator.TileSimpleGenerator;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Created by Geko_X on 3/07/2017.
 *
 */
public class MessageUpdateSimpleGenerator implements IMessage, IMessageHandler<MessageUpdateSimpleGenerator, IMessage> {

	public int currentBurnTime, totalBurnTime;
	public BlockPos pos;

	public MessageUpdateSimpleGenerator() {
		// NO-OP
	}

	public MessageUpdateSimpleGenerator(BlockPos pos, int currentBurnTime, int totalBurnTime) {
		this.pos = pos;
		this.currentBurnTime = currentBurnTime;
		this.totalBurnTime = totalBurnTime;
	}

	@Override
	public IMessage onMessage(MessageUpdateSimpleGenerator message, MessageContext ctx) {

		if(ctx.side == Side.CLIENT) {
			Minecraft.getMinecraft().addScheduledTask(() -> handle(message, ctx));
		}

		return null;
	}

	private void handle(MessageUpdateSimpleGenerator message, MessageContext ctx) {
		World world = Minecraft.getMinecraft().player.getEntityWorld();
		TileSimpleGenerator te = (TileSimpleGenerator) world.getTileEntity(message.pos);

		if(te != null) {
			te.setCurrentBurnTime(message.currentBurnTime);
			te.setTotalBurnTime(message.totalBurnTime);
			te.markDirty();
		}
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.pos = new BlockPos(buf.readInt(), buf.readInt(), buf.readInt());
		this.totalBurnTime = buf.readInt();
		this.currentBurnTime = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.pos.getX());
		buf.writeInt(this.pos.getY());
		buf.writeInt(this.pos.getZ());

		buf.writeInt(this.totalBurnTime);
		buf.writeInt(this.currentBurnTime);

	}

}
