package gekox.electromancy.proxy;


import gekox.electromancy.blocks.ModBlocks;
import gekox.electromancy.client.fx.FXFulmenZap;
import gekox.electromancy.client.render.RenderLightning;
import gekox.electromancy.entity.EntityLightning;
import gekox.electromancy.items.ModItems;
import gekox.electromancy.reference.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.particle.Particle;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);

		OBJLoader.INSTANCE.addDomain(Reference.MODID);
		RenderingRegistry.registerEntityRenderingHandler(EntityLightning.class, RenderLightning::new);

	}

	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);

//		ModItems.initCustomRenders();

	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event) {
		ModBlocks.initRenders();
		ModItems.initRenders();
	}

	@Override
	public EntityPlayer getPlayerFromContext(MessageContext ctx) {
		return Minecraft.getMinecraft().player;
	}

	@Override
	public boolean isHeldShift() {
		return GuiScreen.isShiftKeyDown();
	}
	
	@Override
	public void spawnParticle(World world, BlockPos start, BlockPos end) {
		
		Particle particle = new FXFulmenZap(world, start, end);
		Minecraft.getMinecraft().effectRenderer.addEffect(particle);
		
	}

}
