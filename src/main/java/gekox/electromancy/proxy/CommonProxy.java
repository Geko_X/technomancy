package gekox.electromancy.proxy;

import gekox.electromancy.Electromancy;
import gekox.electromancy.blocks.ModBlocks;
import gekox.electromancy.crafting.LightningCrafting;
import gekox.electromancy.entity.EntityLightning;
import gekox.electromancy.handler.ElectromancyEventHandler;
import gekox.electromancy.items.ModItems;
import gekox.electromancy.network.ElectromancyPacketHandler;
import gekox.electromancy.reference.Names;
import gekox.electromancy.reference.Reference;
import gekox.electromancy.util.LogHelper;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.oredict.OreDictionary;

@Mod.EventBusSubscriber
public class CommonProxy implements IProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {

		MinecraftForge.EVENT_BUS.register(new ElectromancyEventHandler());

	}

	@Override
	public void init(FMLInitializationEvent e) {

		LogHelper.info("Registering ore dictionary strings");
		OreDictionary.registerOre("ingotVoltaicIron", new ItemStack(ModItems.itemVoltaicIngot));
		OreDictionary.registerOre("nuggetVoltaicIron", new ItemStack(ModItems.itemVoltaicNugget));
		OreDictionary.registerOre("blockVoltaicIron", new ItemStack(ModBlocks.blockVoltaicIron));

		ElectromancyPacketHandler.init();

		LogHelper.info("Init'ing lightning recipies");
		LightningCrafting.init();

		LogHelper.info("Registering entities");
		EntityRegistry.registerModEntity(new ResourceLocation(Reference.RESOURCE_PREFIX_NO_COLON, "EntityLightning"), EntityLightning.class, "EntityLightning", 0, Reference.MODID, Names.Entities.LIGHTNING_TRACKING_DISTANCE, Names.Entities.LIGHTING_UPDATE_FREQUENCY, false);

		LogHelper.info("Visualising GUIs");
		NetworkRegistry.INSTANCE.registerGuiHandler(Electromancy.instance, new GuiProxy());
	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {

		//ModItems.initGuideCategories();

	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {

		LogHelper.info("Registering blocks");

		ModBlocks.registerBlock(event);
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {

		LogHelper.info("Registering items");

		ModBlocks.registerItemBlock(event);
		ModItems.registerItem(event);
	}

	@Override
	public EntityPlayer getPlayerFromContext(MessageContext ctx) {

		return null;
	}

	@Override
	public boolean isHeldShift() {
		return false;
	}
	
	@Override
	public void spawnParticle(World world, BlockPos start, BlockPos end) {
		//No-op
	}
}
