package gekox.electromancy.proxy;

import gekox.electromancy.blocks.simpleGenerator.ContainerSimpleGenerator;
import gekox.electromancy.blocks.simpleGenerator.GuiSimpleGenerator;
import gekox.electromancy.blocks.simpleGenerator.TileSimpleGenerator;
import gekox.electromancy.reference.Reference;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

/**
 * Created by Geko_X on 2/07/2017.
 *
 */
public class GuiProxy implements IGuiHandler {

	@Nullable
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {

		BlockPos pos = new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);

		switch(ID) {
			case Reference.GUI.SIMPLE_GENERATOR:
				return new ContainerSimpleGenerator(player.inventory, (TileSimpleGenerator)te);
		}

		return null;
	}

	@Nullable
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		TileEntity te = world.getTileEntity(pos);

		switch(ID) {
			case Reference.GUI.SIMPLE_GENERATOR:
				TileSimpleGenerator tileSimpleGenerator = (TileSimpleGenerator)te;
				return new GuiSimpleGenerator(tileSimpleGenerator, new ContainerSimpleGenerator(player.inventory, (TileSimpleGenerator)te));
		}

		return null;
	}
}
