package gekox.electromancy.proxy;



//Interface to enforce the proxy methods

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public interface IProxy {

	// Pre init
	public abstract void preInit(FMLPreInitializationEvent e);

	// Init
	public abstract void init(FMLInitializationEvent e);

	// Post init
	public abstract void postInit(FMLPostInitializationEvent e);

	public abstract EntityPlayer getPlayerFromContext(MessageContext ctx);

	public boolean isHeldShift();
	
	public void spawnParticle(World world, BlockPos start, BlockPos end);
}

