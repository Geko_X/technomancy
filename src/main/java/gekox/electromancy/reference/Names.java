package gekox.electromancy.reference;

/**
 * Created by Geko_X on 4/10/2016.
 *
 */
public class Names {
	
	public class Items {

		public static final String GUIDE_BOOK = "guideBook";
		public static final String MATERIAL = "material";

		public static final String VOLTAIC_IRON_INGOT = "voltaic_iron_ingot";
		public static final String VOLTAIC_IRON_NUGGET = "voltaic_iron_nugget";
		
		public static final String POWER_STORAGE = "power_storage";
		public static final String POTENTIOMETER = "potentiometer";
		public static final String LIGHTNING = "lightning";

		public static final String VOLTAIC_SWORD = "voltaic_sword";
		public static final String VOLTAIC_PICK = "voltaic_pick";
		public static final String VOLTAIC_AXE = "voltaic_axe";
		public static final String VOLTAIC_SHOVEL = "voltaic_shovel";
		public static final String VOLTAIC_HOE = "voltaic_hoe";
		public static final String VOLTAIC_SHEARS = "voltaic_shears";
	}
	
	public class Blocks {
		
		public static final String VOLTAIC_IRON_BLOCK = "voltaic_iron_block";
		public static final String RELAY = "relay";
		public static final String STORAGE = "storage";
		public static final String SIMPLE_GENERATOR = "simple_generator";
		
	}
	
	public class Materials {

	}
	
	public class Entities {

		public static final String LIGHTNING_NAME = "lightning";
		public static final int LIGHTNING_TRACKING_DISTANCE = 64;
		public static final int LIGHTING_UPDATE_FREQUENCY = 20;
	}

	public static final String[] MATERIALS = {Items.VOLTAIC_IRON_INGOT,
											  Items.VOLTAIC_IRON_NUGGET};

}
