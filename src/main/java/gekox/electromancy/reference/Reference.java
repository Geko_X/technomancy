package gekox.electromancy.reference;

/**
 * Created by Geko_X on 4/10/2016.
 *
 */
public class Reference {

	public static final String NAME = "Electromancy";
	public static final String MODID = "electromancy";
	public static final String RESOURCE_PREFIX = MODID.toLowerCase() + ":";
	public static final String RESOURCE_PREFIX_NO_COLON = MODID.toLowerCase();
	public static final String VERSION = "0.1";
	public static final String DEPENDENTS = "after:JEI";

	public static final String CLIENT_PROXY_CLASS = "gekox.electromancy.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "gekox.electromancy.proxy.ServerProxy";
	
	public static final String GUIDE_BASE_CATEGORY = "guide.electromancy.category.";
	public static final String GUIDE_BASE_ENTRY = "guide.electromancy.entry.";
	
	public static final String NBT_POWER = MODID + "Power";
	public static final String NBT_POWER_MAX = MODID + "MaxPower";
	public static final String NBT_ITEMS = MODID + "Items";
	public static final String NBT_TOTAL_WORK = MODID + "TotalWork";
	public static final String NBT_CURRENT_WORK = MODID + "CurrentWork";

	public static final String JEI_LIGHTNING = MODID + ".lightning";
	public static final String JEI_DESCRIPTION = "jei." + MODID + ".description.";

	public static class GUI {
		public static final int SIMPLE_GENERATOR = 1;
	}

}
