package gekox.electromancy.reference;

import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

/**
 * Created by Geko_X on 24/10/2016.
 *
 */
public class Values {

	// How much energy is needed to craft something
	public class Crafting {
		public static final double VOLTAIC_IRON_POTENTIAL = 5;
		public static final double VOLTAIC_TOOLS = 10;

	}

	// How much energy is generated
	public class Generating {
		public static final double EXPLOSION = 50;
		public static final double BURNING_GENERATOR = 0.1;
	}

	public static class Tools {
		// Name, harvest level, durability, mining speed, damage, enchantability
		// Iron: 2, 250, 6.0, 2.0 (4 + 2), 14
		public static Item.ToolMaterial VOLTAIC_IRON = EnumHelper.addToolMaterial("VOLTAIC_IRON", 2, 300, 2.0f, 5.0f, 14);
	}

	public class Misc {

		// Threasholds for dangerous PE events
		public static final double POTENTIAL_DANGER_LOW = 50;
		public static final double POTENTIAL_DANGER_HIGH = 200;
	}

}
