package gekox.electromancy.util;

public class Color {
	
	// Color used by vanilla minecraft for guis and whatnot
	public static final int MAGIC_COLOR = 4210752;
	
	public static Color WHITE = new Color(221, 221, 221);
	public static Color ORANGE = new Color(219, 125, 62);
	public static Color MAGENTA = new Color(179, 80, 188);
	public static Color LIGHT_BLUE = new Color(107, 138, 201);
	public static Color YELLOW = new Color(177, 166, 39);
	public static Color LIME = new Color(65, 174, 56);
	public static Color PINK = new Color(208, 132, 153);
	public static Color GRAY = new Color(64, 64, 64);
	public static Color LIGHT_GRAY = new Color(154, 161, 161);
	public static Color CYAN = new Color(46, 110, 137);
	public static Color PURPLE = new Color(126, 61, 181);
	public static Color BLUE = new Color(46, 56, 141);
	public static Color BROWN = new Color(79, 50, 31);
	public static Color GREEN = new Color(53, 70, 27);
	public static Color RED = new Color(150, 52, 48);
	public static Color BLACK = new Color(25, 22, 22);
	public static Color CLEAR = new Color(255, 255, 255, 0);

	
	public int r;
	public int g;
	public int b;
	public int a;
	
	public static final Color[] color = {WHITE, ORANGE, MAGENTA, LIGHT_BLUE, YELLOW, LIME, PINK, GRAY, LIGHT_GRAY, CYAN, PURPLE, BLUE, BROWN, GREEN, RED, BLACK};

	public Color(int r, int g, int b, int a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(int r, int g, int b) {
		this(r, g, b, 255);
	}
	
	public Color() {
		this(0, 0, 0);
	}
	
	// http://stackoverflow.com/questions/4801366/convert-rgb-values-into-integer-pixel
	public int toInt() {
		return ((this.r&0x0ff)<<16)|((this.g&0x0ff)<<8)|(this.b&0x0ff);
	}
	
	@Override
	public String toString() {
		return String.format("(%d, %d, %d, %d)", r, g, b, a);
	}
	
	@Override
	public boolean equals(Object other) {
		
		if(other instanceof Color) {
			Color c2 = (Color)other;
			return (this.r == c2.r && this.g == c2.g && this.b == c2.b && this.a == c2.a);
		}
		
		return false;
	}
	
	public static Color lerp(Color start, Color end, float t) {
		
		Color c1 = start.copy();
		Color c2 = end.copy();

		Color result = new Color();

		result.r = (int) (c2.r * t + c1.r * (1 - t));
		result.g = (int) (c2.g * t + c1.g * (1 - t));
		result.b = (int) (c2.b * t + c1.b * (1 - t));
		result.a = (int) (c2.a * t + c1.a * (1 - t));
		
		return result;
		
	}
	
	public Color copy() {
		return new Color(this.r, this.g, this.b, this.a);
	}
}