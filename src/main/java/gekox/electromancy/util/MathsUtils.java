package gekox.electromancy.util;

/**
 * Created by Geko_X on 19/10/2016.
 *
 */
public class MathsUtils {
	
	public static boolean almostEqual(double a, double b, double eps){
		return Math.abs(a - b) < eps;
	}
	
}
