package gekox.electromancy.util;

import gekox.electromancy.reference.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

import java.util.function.IntFunction;

/**
 * Created by Geko_X on 17/10/2016.
 *
 */
public class ModelHelper {
	
	public static void registerItemAllMeta(Item item, int range) {
		registerItemMetas(item, range, i -> item.getRegistryName().getResourcePath());
	}
	
	public static void registerItemAppendMeta(Item item, int maxExclusive, String loc) {
		registerItemMetas(item, maxExclusive, i -> loc + i);
	}
	
	public static void registerItemMetas(Item item, int maxExclusive, IntFunction<String> metaToName) {
		for (int i = 0; i < maxExclusive; i++) {
			ModelLoader.setCustomModelResourceLocation(
					item, i,
					new ModelResourceLocation(Reference.RESOURCE_PREFIX + metaToName.apply(i), "inventory")
			);
		}
	}
	
}
