package gekox.electromancy.util;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.VertexBuffer;

/**
 * Created by Geko_X on 18/10/2016.
 *
 * Cheers McJty!
 */
public class RenderHelper {
	
	public static void drawQuad(Tessellator tessellator, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4) {
		int brightness = 240;
		int b1 = brightness >> 16 & 65535;
		int b2 = brightness & 65535;

		BufferBuilder buffer = tessellator.getBuffer();
		buffer.pos(p1.getX(), p1.getY(), p1.getZ()).tex(0.0D, 0.0D).lightmap(b1, b2).color(255, 255, 255, 128).endVertex();
		buffer.pos(p2.getX(), p2.getY(), p2.getZ()).tex(1.0D, 0.0D).lightmap(b1, b2).color(255, 255, 255, 128).endVertex();
		buffer.pos(p3.getX(), p3.getY(), p3.getZ()).tex(1.0D, 1.0D).lightmap(b1, b2).color(255, 255, 255, 128).endVertex();
		buffer.pos(p4.getX(), p4.getY(), p4.getZ()).tex(0.0D, 1.0D).lightmap(b1, b2).color(255, 255, 255, 128).endVertex();
	}
	
	public static void drawBeam(Vector3 start, Vector3 end, Vector3 player, float width) {
		
		Vector3 PS = Vector3.Subtract(start, player);
		Vector3 SE = Vector3.Subtract(end, start);
		Vector3 normal = Vector3.CrossProduct(PS, SE);
		normal = normal.normalize();
		
		Vector3 half  = Vector3.Multiply(normal, width);
		
		Vector3 p1 = Vector3.Add(start, half);
		Vector3 p2 = Vector3.Subtract(start, half);
		Vector3 p3 = Vector3.Add(end, half);
		Vector3 p4 = Vector3.Subtract(end, half);
		
		
		
	}

}
