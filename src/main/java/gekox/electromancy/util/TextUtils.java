package gekox.electromancy.util;

import net.minecraft.util.text.translation.I18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

public class TextUtils {

	public static final String CODE_BLACK = (char) 167 + "0";
	public static final String CODE_BLUE = (char) 167 + "1";
	public static final String CODE_GREEN = (char) 167 + "2";
	public static final String CODE_CYAN = (char) 167 + "3";
	public static final String CODE_RED = (char) 167 + "4";
	public static final String CODE_PURPLE = (char) 167 + "5";
	public static final String CODE_ORANGE = (char) 167 + "6";
	public static final String CODE_LIGHT_GRAY = (char) 167 + "7";
	public static final String CODE_GRAY = (char) 167 + "8";
	public static final String CODE_LIGHT_BLUE = (char) 167 + "9";
	public static final String CODE_LIME = (char) 167 + "a";
	public static final String CODE_BRIGHT_BLUE = (char) 167 + "b";
	public static final String CODE_PINK = (char) 167 + "c";
	public static final String CODE_MAGENTA = (char) 167 + "d";
	public static final String CODE_YELLOW = (char) 167 + "e";
	public static final String CODE_WHITE = (char) 167 + "f";
	public static final String CODE_OBFUSCATED = (char) 167 + "k";
	public static final String CODE_CODE_BOLD = (char) 167 + "l";
	public static final String CODE_STRIKETHROUGH = (char) 167 + "m";
	public static final String CODE_UNDERLINE = (char) 167 + "n";
	public static final String CODE_ITALIC = (char) 167 + "o";
	public static final String CODE_END = (char) 167 + "r";
	
	public static final String[] COLOR_CODES = {
			CODE_WHITE,
			CODE_ORANGE,
			CODE_MAGENTA,
			CODE_LIGHT_BLUE,
			CODE_YELLOW,
			CODE_LIME,
			CODE_PINK,
			CODE_GRAY,
			CODE_LIGHT_GRAY,
			CODE_CYAN,
			CODE_PURPLE,
			CODE_BLUE,
			CODE_ORANGE,
			CODE_GREEN,
			CODE_RED,
			CODE_GRAY};
	
	public static String getCodeFromColor(Color color) {
		
		for(int i = 0; i < Color.color.length; i++) {
			if(color.equals(Color.color[i])) {
				return COLOR_CODES[i];
			}
		}
		
		return "";
		
	}
	
	public static final String[] COLOR_NAMES = {
		"white",
		"orange",
		"magenta",
		"lightBlue",
		"yellow",
		"lime",
		"pink",
		"gray",
		"lightGray",
		"cyan",
		"purple",
		"blue",
		"brown",
		"green",
		"red",
		"black"
	};
	
	public static String getNameFromColor(Color color) {
		
		for(int i = 0; i < Color.color.length; i++) {
			if(color.equals(Color.color[i])) {
				return COLOR_NAMES[i];
			}
		}
		
		return "";
		
	}
	
	public static final NavigableMap<Long, String> suffix = new TreeMap<>();
	static {
		suffix.put(1_000L, "k");
		suffix.put(1_000_000L, "M");
		suffix.put(1_000_000_000L, "G");
		suffix.put(1_000_000_000_000L, "T");
		suffix.put(1_000_000_000_000_000L, "P");
		suffix.put(1_000_000_000_000_000_000L, "E");
	}
	
	// http://stackoverflow.com/questions/1086123/string-conversion-to-title-case
	public static final String toTitleCase(String text) {
		
		StringBuilder titleCase = new StringBuilder();
		text = text.toLowerCase();
		boolean nextIsUpper = true;
		
		for(char c : text.toCharArray()) {
			if(Character.isWhitespace(c))
				nextIsUpper = true;
			else {
				c = Character.toTitleCase(c);
				nextIsUpper = false;
			}
			
			titleCase.append(c);
		}
		
		return titleCase.toString();
	}
	
	// http://stackoverflow.com/questions/4753251/how-to-go-about-formatting-1200-to-1-2k-in-java
	public static String formatNumber(long num) {
		if (num == Long.MIN_VALUE)
			return formatNumber(Long.MIN_VALUE + 1);
		if (num < 0)
			return "-" + formatNumber(-num);
		if (num < 1000)
			return Long.toString(num);

		Entry<Long, String> e = suffix.floorEntry(num);
		Long divideBy = e.getKey();
		String suffix = e.getValue();

		long truncated = num / (divideBy / 10);
		boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
		return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
	}
	
	public static String getCodeForPercent(float percent) {
		return getCodeForPercent((double) percent);
	}
	
	public static String getCodeForPercent(double percent) {
		if(percent < 0.1)
			return CODE_RED;
		else if(percent < 0.5)
			return CODE_YELLOW;
		else
			return CODE_GREEN;
	}
	
	public static String getFormattedText(String string) {
		return string.replaceAll("&", "\u00A7");
	}

	public static String localize(String input, Object... format) {
		return I18n.translateToLocalFormatted(input, format);
	}

	public static String localizeEffect(String input, Object... format) {
		return getFormattedText(localize(input, format));
	}

	public static String[] localizeAll(String[] input) {
		String[] ret = new String[input.length];
		for (int i = 0; i < input.length; i++)
			ret[i] = localize(input[i]);

		return ret;
	}

	public static String[] localizeAllEffect(String[] input) {
		String[] ret = new String[input.length];
		for (int i = 0; i < input.length; i++)
			ret[i] = localizeEffect(input[i]);

		return ret;
	}

	public static ArrayList<String> localizeAll(List<String> input) {
		ArrayList<String> ret = new ArrayList<String>(input.size());
		for (int i = 0; i < input.size(); i++)
			ret.add(i, localize(input.get(i)));

		return ret;
	}

	public static ArrayList<String> localizeAllEffect(List<String> input) {
		ArrayList<String> ret = new ArrayList<String>(input.size());
		for (int i = 0; i < input.size(); i++)
			ret.add(i, localizeEffect(input.get(i)));

		return ret;
	}

	
	
	
}
