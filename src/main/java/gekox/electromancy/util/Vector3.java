package gekox.electromancy.util;

import net.minecraft.util.math.BlockPos;

/**
 * Created by Geko_X on 18/10/2016.
 *
 */
public class Vector3 {
	
	public final float x;
	public final float y;
	public final float z;
	
	public Vector3(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3(double x, double y, double z) {
		this.x = (float)x;
		this.y = (float)y;
		this.z = (float)z;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getZ() {
		return z;
	}
	
	public float norm() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}
	
	public static Vector3 FromBlockPos(BlockPos pos) {
		return new Vector3(pos.getX(), pos.getY(), pos.getZ());
	}
	
	public Vector3 normalize() {
		float n = norm();
		return new Vector3(x / n, y / n, z / n);
	}
	
	public static Vector3 CrossProduct(Vector3 a, Vector3 b) {
		float x = a.y*b.z - a.z*b.y;
		float y = a.z*b.x - a.x*b.z;
		float z = a.x*b.y - a.y*b.x;
		return new Vector3(x, y, z);
	}
	
	public static double DistanceSq(Vector3 a, Vector3 b) {
		return Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2) + Math.pow(b.z - a.z, 2);
	}
	
	public static double Distance(Vector3 a, Vector3 b) {
		return Math.sqrt(DistanceSq(a, b));
	}
	
	public static Vector3 Subtract(Vector3 a, Vector3 b) {
		return new Vector3(a.x-b.x, a.y-b.y, a.z-b.z);
	}
	
	public static Vector3 Add(Vector3 a, Vector3 b) {
		return new Vector3(a.x+b.x, a.y+b.y, a.z+b.z);
	}
	
	public static Vector3 Multiply(Vector3 a, float f) {
		return new Vector3(a.x * f, a.y * f, a.z * f);
	}
	
	public static Vector3 Divide(Vector3 a, float f) {
		return new Vector3(a.x / f, a.y / f, a.z / f);
	}
	
}
