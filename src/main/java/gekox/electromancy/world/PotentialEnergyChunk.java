package gekox.electromancy.world;

import net.minecraft.util.math.ChunkPos;

/**
 * Created by Geko_X on 14/10/2016.
 *
 */
public class PotentialEnergyChunk {

	private ChunkPos pos;
	private double potential = 0;
//	private WeakReference<Chunk> chunk;

	public double getPotential() {
		return this.potential;
	}

	public void setPotential(double potential) {
		this.potential = potential;
	}

	public ChunkPos getPos() {
		return pos;
	}

	public PotentialEnergyChunk(ChunkPos pos, double potential) {
		this.pos = pos;
//		this.chunk = new WeakReference<>(chunk);

		this.potential = potential;
	}

	public PotentialEnergyChunk(ChunkPos pos) {
		this(pos, 0);
	}

//	public boolean isModified() {
//		if ((this.chunk != null) && (this.chunk.get() != null)) {
//			return this.chunk.get().needsSaving(false);
//		}
//
//		return false;
//	}
}
