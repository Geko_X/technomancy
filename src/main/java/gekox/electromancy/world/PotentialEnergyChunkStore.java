package gekox.electromancy.world;

import gekox.electromancy.reference.Values;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Geko_X on 14/10/2016.
 *
 */
public class PotentialEnergyChunkStore {

	private static ConcurrentHashMap<Integer, PotentialEnergyWorld> worldChunks = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, CopyOnWriteArrayList<ChunkPos>> dirtyChunks = new ConcurrentHashMap<>();

	private static Random random = new Random();

	public static PotentialEnergyChunk getPotentialChunkForWorld(int dim, int x, int z) {

		PotentialEnergyWorld pworld = getPotentialEnergyWorld(dim);
		PotentialEnergyChunk pchunk = pworld.getPotentialEnergyChunk(x, z);

		return pchunk;

	}

	public static PotentialEnergyWorld getPotentialEnergyWorld(int dim) {

		if(!worldChunks.containsKey(dim)) {
			addWorld(dim);
		}

		return worldChunks.get(dim);

	}

	public static ConcurrentHashMap<ChunkPos, PotentialEnergyChunk> getAllChunksForWorld(int dim) {
		return getPotentialEnergyWorld(dim).getChunks();
	}

	public static double getPotential(int dim, int x, int z) {
		PotentialEnergyChunk pchunk = getPotentialChunkForWorld(dim, x, z);
		return pchunk.getPotential();
	}

	public static double getPotential(World world, BlockPos pos) {
		return getPotential(world.provider.getDimension(), pos.getX() >> 4, pos.getZ() >> 4);
	}

	public static void addWorld(int dim, PotentialEnergyWorld pworld) {
		worldChunks.put(dim, pworld);
	}

	public static void addWorld(int dim) {
		PotentialEnergyWorld pworld = new PotentialEnergyWorld(dim);
		addWorld(dim, pworld);
	}

	public static void addChunk(int dim, ChunkPos pos, double potential) {
		PotentialEnergyWorld pworld = getPotentialEnergyWorld(dim);
		pworld.addChunk(pos.x, pos.z, potential);

		if(!dirtyChunks.containsKey(dim)) {
			dirtyChunks.put(dim, new CopyOnWriteArrayList<>());
		}
		dirtyChunks.get(dim).add(pos);
	}

	public static void addChunk(int dim, int x, int z, double potential) {
		addChunk(dim, new ChunkPos(x, z), potential);
	}

	/* Add and remove */

	public static double addPotential(int dim, int x, int z, double amount) {
		double potential = getPotential(dim, x, z);
		potential += amount;
		addChunk(dim, x, z, potential);
		return amount;
	}

	public static double addPotential(World world, BlockPos pos, double amount) {
		int dim = world.provider.getDimension();
		return addPotential(dim, pos.getX() >> 4, pos.getZ() >> 4, amount);
	}

	public static double removePotential(int dim, int x, int z, double amount) {
		double potential = getPotential(dim, x, z);
		amount = Math.min(potential, amount);
		potential -= amount;
		addChunk(dim, x, z, potential);
		return amount;
	}

	public static double removePotential(World world, BlockPos pos, double amount) {
		int dim = world.provider.getDimension();
		return removePotential(dim, pos.getX() >> 4, pos.getZ() >> 4, amount);
	}

	/* Effects */
	public static void processPotentialChunk(int dim, int x, int z) {

		double potential = getPotential(dim, x, z);

		// TODO: Config values for these

		// Explosions
		// Guarenteed at PE = 250
		if(potential > Values.Misc.POTENTIAL_DANGER_LOW) {

			if(potential > random.nextInt((int)Values.Misc.POTENTIAL_DANGER_LOW * 5)) {

				World world = DimensionManager.getWorld(dim);
				BlockPos pos = new BlockPos(x * 16 + random.nextInt(16), random.nextInt(world.getHeight()), z * 16 + random.nextInt(16));

				world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), 3, false);
				removePotential(dim, x, z, 10);

			}
		}

		// Minimum for lightning
		// Guarenteed at PE = 1000
		if(potential > Values.Misc.POTENTIAL_DANGER_HIGH) {

			if(potential > random.nextInt((int)Values.Misc.POTENTIAL_DANGER_HIGH * 5)) {

				World world = DimensionManager.getWorld(dim);
				BlockPos pos = new BlockPos(x * 16 + random.nextInt(16), 64, z * 16 + random.nextInt(16));
				int y = world.getTopSolidOrLiquidBlock(pos).getY();

				world.addWeatherEffect(new EntityLightningBolt(world, pos.getX(), y + 2, pos.getZ(), false));
				removePotential(dim, x, z, 100);
			}
		}
	}

	public static void processPotentialChunk(int dim, ChunkPos pos) {
		processPotentialChunk(dim, pos.x, pos.z);
	}

	public static void processPotentialChunk(World world, BlockPos pos) {
		processPotentialChunk(world.provider.getDimension(), pos.getX() >> 4, pos.getZ() >> 4);
	}

}
