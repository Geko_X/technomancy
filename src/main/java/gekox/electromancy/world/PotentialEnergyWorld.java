package gekox.electromancy.world;

import gekox.electromancy.util.LogHelper;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.DimensionManager;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Geko_X on 14/10/2016.
 *
 */
public class PotentialEnergyWorld {

	private ConcurrentHashMap<ChunkPos, PotentialEnergyChunk> chunks = new ConcurrentHashMap<>();
	private int dim;
	private Random random = new Random();

	public PotentialEnergyWorld(int dim) {
		this.dim = dim;
		random.setSeed(DimensionManager.getWorld(dim).getWorldInfo().getSeed());
	}

	public ConcurrentHashMap<ChunkPos, PotentialEnergyChunk> getChunks() {
		return chunks;
	}

	public void addChunk(int x, int z, double potentialEnergy) {

		ChunkPos pos = new ChunkPos(x, z);

		PotentialEnergyChunk pchunk = new PotentialEnergyChunk(pos, potentialEnergy);
		this.chunks.put(pos, pchunk);
	}

	public void addChunk(ChunkPos pos, double potentialEnergy) {
		this.addChunk(pos.x, pos.z, potentialEnergy);
	}

	public void addChunk(ChunkPos pos) {
		this.generatePotential(pos);
	}

	public PotentialEnergyChunk getPotentialEnergyChunk(ChunkPos pos) {

		if(!this.chunks.containsKey(pos)) {
			this.addChunk(pos);
		}

		return this.chunks.get(pos);
	}

	public PotentialEnergyChunk getPotentialEnergyChunk(Chunk chunk) {
		ChunkPos pos = chunk.getPos();
		return this.getPotentialEnergyChunk(pos);
	}

	public PotentialEnergyChunk getPotentialEnergyChunk(int x, int z) {
		ChunkPos pos = new ChunkPos(x, z);
		return this.getPotentialEnergyChunk(pos);
	}

	public void removeChunk(Chunk chunk) {
		ChunkPos pos = chunk.getPos();
		this.chunks.remove(pos);
	}

	public void generatePotential(ChunkPos pos) {

		double potential = random.nextDouble();

		addChunk(pos, potential);

		String str = String.format("Potential energy generated for world(%d) chunk(%d, %d): %f", dim, pos.x, pos.z, potential);
		LogHelper.info(str);

	}
}
