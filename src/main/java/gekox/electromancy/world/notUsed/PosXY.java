package gekox.electromancy.world.notUsed;

/***
 * WayOfTime's PosXY class
 *
 * https://github.com/WayofTime/BloodMagic/blob/1.9/src/main/java/WayofTime/bloodmagic/demonAura/PosXY.java
 ***/

public class PosXY implements Comparable<PosXY> {
	public int x;
	public int y;

	public PosXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 59;
		int result = 1;
		final long temp1 = Double.doubleToLongBits(this.x) + Double.doubleToLongBits(this.y);
		result = (result * PRIME) + (int) (temp1 ^ (temp1 >>> 32));
		return result;
	}
			
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PosXY) {
			PosXY other = (PosXY)obj;
			return (this.x == other.x && this.y == other.y);
		}
		
		return false;
	}
	
	@Override
	public int compareTo(PosXY c) {
		return this.y == c.y ? this.x - c.x : this.y - c.y;
	}

	@Override
	public String toString() {
		return String.format("PosXY at (%d, %d)", x, y);
	}

	public float getDistanceSquared(int x, int z) {
		float f = this.x - x;
		float f2 = this.y - z;
		return f * f + f2 * f2;
	}

	public float getDistanceSquaredToChunkCoordinates(PosXY c) {
		return getDistanceSquared(c.x, c.y);
	}
}
