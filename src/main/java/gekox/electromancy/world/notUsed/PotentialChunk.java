package gekox.electromancy.world.notUsed;

import net.minecraft.world.chunk.Chunk;

import java.lang.ref.WeakReference;

/**
 * Created by Geko_X on 13/10/2016.
 *
 * Based on WayOfTime's WillChunk
 * https://github.com/WayofTime/BloodMagic/blob/1.9/src/main/java/WayofTime/bloodmagic/demonAura/WillChunk.java
 *
 */
public class PotentialChunk {

	public PosXY pos;
	private double potential = 0;
	private WeakReference<Chunk> chunk;

	public double getPotential() {
		return this.potential;
	}

	public void setPotential(double potential) {
		this.potential = potential;
	}

	public PotentialChunk(PosXY pos) {
		this.pos = pos;
	}

	public PotentialChunk(Chunk chunk, double potential) {
		this.pos = new PosXY(chunk.x, chunk.z);
		this.chunk = new WeakReference<>(chunk);
		this.potential = potential;
	}

	public boolean isModified() {
		if ((this.chunk != null) && (this.chunk.get() != null)) {
			return this.chunk.get().needsSaving(false);
		}

		return false;
	}

}
