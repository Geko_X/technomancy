package gekox.electromancy.world.notUsed;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Geko_X on 13/10/2016.
 *
 * Based on WayOfTime's WillWorld
 * https://github.com/WayofTime/BloodMagic/blob/1.9/src/main/java/WayofTime/bloodmagic/demonAura/WillWorld.java
 *
 */
public class PotentialWorld {

	private int dim;
	private ConcurrentHashMap<PosXY, PotentialChunk> potentialChunks = new ConcurrentHashMap<>();

	public ConcurrentHashMap<PosXY, PotentialChunk> getPotentialChunks() {
		return potentialChunks;
	}

	public void setPotentialChunks(ConcurrentHashMap<PosXY, PotentialChunk> potentialChunks) {
		this.potentialChunks = potentialChunks;
	}

	public PotentialWorld(int dim) {
		this.dim = dim;
	}

	public PotentialChunk getChunkAt(int x, int z) {
		return getChunkAt(new PosXY(x, z));
	}

	public PotentialChunk getChunkAt(PosXY pos) {
		return this.potentialChunks.get(pos);
	}



}
