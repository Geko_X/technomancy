package gekox.electromancy.world.notUsed;

import gekox.electromancy.util.LogHelper;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Geko_X on 13/10/2016.
 *
 *
 * Based on WayOfTime's WorldDemonWillHandler
 * https://github.com/WayofTime/BloodMagic/blob/1.9/src/main/java/WayofTime/bloodmagic/demonAura/WorldDemonWillHandler.java
 */
public class WorldPotentialHandler {

	private static ConcurrentHashMap<Integer, PotentialWorld> potentialWorldMap = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<Integer, CopyOnWriteArrayList<PosXY>> dirtyChunks = new ConcurrentHashMap<>();

	public static double getPotential(int dim, int x, int z) {
		PotentialChunk chunk = getPotentialChunk(dim, x, z);

		if (chunk != null)
			return chunk.getPotential();

		return 0;
	}

	public static double getPotential(World world, BlockPos pos) {
		Chunk chunk = world.getChunkFromBlockCoords(pos);
		return getPotential(world.provider.getDimension(), chunk.x, chunk.z);
	}


	public static PotentialWorld getPotenialWorld(int dim) {
		return potentialWorldMap.get(dim);
	}

	public static PotentialChunk getPotentialChunk(int dim, int x, int z) {
		if (!potentialWorldMap.containsKey(dim)) {
			addPotentialWorld(dim);
		}

		PotentialChunk pChunk = (potentialWorldMap.get(dim)).getChunkAt(x, z);
		return pChunk;
	}

	public static PotentialChunk getPotentialChunk(World world, BlockPos pos) {

		Chunk chunk = world.getChunkFromBlockCoords(pos);
		PotentialChunk potentialChunk = getPotentialChunk(world.provider.getDimension(), chunk.x, chunk.z);

		if (potentialChunk == null) {
			generatePotential(chunk);
			potentialChunk = getPotentialChunk(world.provider.getDimension(), chunk.x, chunk.z);
		}

		return potentialChunk;
	}

	public static void addPotentialWorld(int dim) {
		if (!potentialWorldMap.containsKey(dim)) {
			potentialWorldMap.put(dim, new PotentialWorld(dim));
			LogHelper.info("Added potential energy information for world " + dim);
		}
	}

	public static void removePotentialWorld(int dim) {
		potentialWorldMap.remove(dim);
	}

	public static void addPotentialChunk(int dim, Chunk chunk, double potential) {
		PotentialWorld world = potentialWorldMap.get(dim);
		if (world == null) {
			world = new PotentialWorld(dim);
		}
		world.getPotentialChunks().put(new PosXY(chunk.x, chunk.z), new PotentialChunk(chunk, potential));

		potentialWorldMap.put(dim, world);
	}

	public static void removePotentialChunk(int dim, int x, int y) {
		PotentialWorld world = potentialWorldMap.get(dim);
		if (world != null) {
			PotentialChunk chunk = world.getPotentialChunks().remove(new PosXY(x, y));
			if (chunk != null) {
				markChunkAsDirty(chunk, dim);
			}
		}
	}

	public static double reducePotential(World world, BlockPos pos, double amount, boolean simulate) {

		PotentialChunk chunk = getPotentialChunk(world, pos);
		double potential = chunk.getPotential();
		double reduction = Math.min(potential, amount);

		if(!simulate) {
			potential -= reduction;
			chunk.setPotential(potential);
			markChunkAsDirty(chunk, world.provider.getDimension());
		}

		return reduction;
	}

	public static double increasePotential(World world, BlockPos pos, double amount, boolean simulate) {

		LogHelper.info("Increasing potential!");

		PotentialChunk chunk = getPotentialChunk(world, pos);
		double potential = chunk.getPotential();

		if (!simulate) {
			potential += amount;
			chunk.setPotential(potential);
			markChunkAsDirty(chunk, world.provider.getDimension());
		}

		return amount;
	}

	private static void markChunkAsDirty(PotentialChunk chunk, int dim) {
		if (chunk.isModified()) {
			return;
		}

		PosXY pos = new PosXY(chunk.pos.x, chunk.pos.y);
		if (!dirtyChunks.containsKey(dim)) {
			dirtyChunks.put(dim, new CopyOnWriteArrayList<>());
		}

		CopyOnWriteArrayList<PosXY> dc = dirtyChunks.get(dim);
		if (!dc.contains(pos)) {
			dc.add(pos);
		}
	}

	public static void generatePotential(Chunk chunk) {

//		Random random = new Random();
//		random.setSeed(chunk.xPosition + chunk.zPosition + chunk.getWorld().getSeed());
//
//		double potential = random.nextDouble();
		double potential = 1;

		addPotentialChunk(chunk.getWorld().provider.getDimension(), chunk, potential);

		String str = String.format("Potential energy generated for chunk at (%d, %d): %f", chunk.x, chunk.z, potential);
		LogHelper.info(str);

	}

}
